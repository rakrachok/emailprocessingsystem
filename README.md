**Email processing system**

The entire system consists of the following modules:

* app - Spring Boot web-based module which represents an API for putting emails to HDFS 
* core - the module contains generic and common interface to start development from the very beginning. It is some kind of a spec for other modules and the system itself. 
* email-processing - Spark Streaming module is responsible for picking all algorithm based extractors and perform streaming operation
* email-storage - the module is created for performing communication between storages like HDFS, SQL and perform non-straeming operations

Technology stack:

* Spark 2 (Core, Streaming, Sql)
* Spring boot
* Hadooop
* Docker (https://hub.docker.com/r/sequenceiq/hadoop-docker/)
* MySql database

Hadoop based cluster hosted preferably on the default host and port (localhost:9000).

Unfortunately, due to limited time frame I have not done every single stuff I want to.
There are some task/improvements which need to be done

1. Migrate to NoSQL db instead of MySQL. HBase is much easier to work with in terms of this test task;
2. Get rid of explicit managers/composite features;
3. Value types of the features and artifacts should be Optional in order not to struggle with NullChecks;
4. Integration tests for all 3 pipeline operations.

I really enjoyed working on this task. I hope you will like it