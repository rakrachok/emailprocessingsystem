**Email processing system**

The goal is to design and implement a prototype to store and process emails. The base stack of technologies is the following:

* Hadoop/HBase
* MySql
* Elasticsearch
* Spark
* Or composition of the above

Size of an email:

* < 10Mb w/o attachment
* with attachment > 10Mb

Processing steps:

1. Store an original email in the DB;
2. Artifacts extraction. This step requires an access to the original .eml file. Email content analysis is performed. As a result, artifact should be extracted and stored. E.g. sender mentioned finance instruments “ORCL” in a email content. As a result, “ORCL” and its pos in a content should be saved in an association with this email;
3. Primary feature calculation. This step requires an access to email data like date, content length, subject, CC/TO fields and to artifacts from the previous step. The result of this step is a numeric feature that is associated with email. E.g. for 1.eml;
4. Secondary feature calculation. This step requires an access to the primary feature set and calculates derivatives.