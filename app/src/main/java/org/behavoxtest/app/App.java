package org.behavoxtest.app;

import org.springframework.boot.SpringApplication;

/**
 * Spring boot app which runs web app server + Spark
 */
public class App {
    public static void main(String[] args) {
        SpringApplication.run(AppConfiguration.class, args);
    }
}
