package org.behavoxtest.app;

import org.behavoxtest.storage.EmailStorageConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.behavoxtest.processing.EmailProcessingConfiguration;

/**
 * Main application configuration which gathers common configs from processing and storage modules
 */
@Configuration
@EnableAutoConfiguration
@Import({EmailProcessingConfiguration.class, EmailStorageConfiguration.class})
@ComponentScan("org.behavoxtest.*")
public class AppConfiguration {
}
