package org.behavoxtest.app.controller;


import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.behavoxtest.core.filestorage.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * File Controller to upload the file to HDFS incoming directory
 * For local testing: curl -F fileLocator="emlfile" -F file=@/path/to/emlfile.eml http://localhost:8080/upload
 */
@RestController
@RequestMapping("/upload")
public class FileController {

    private static final Logger LOG = LoggerFactory.getLogger(FileController.class);

    private FileStorageService storageService;

    @Value("${app.tempstorage.path}")
    private String tempStoragePath;

    @Autowired
    public FileController(FileStorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping
    public void post(@RequestParam("fileLocator") String fileLocator, @RequestParam("file") MultipartFile file) throws IOException, DataAccessException {
        LOG.info("Uploading file via HTTP: fileLocator: {}; fileName: {}", fileLocator, file.getOriginalFilename());
        Path path = Paths.get(tempStoragePath);
        Path filePath = Paths.get(tempStoragePath + System.getProperty("file.separator") + file.getOriginalFilename());
        try {
            file.transferTo(path);
            storageService.persist(fileLocator, path.toUri());
        } finally {
            Files.deleteIfExists(filePath);
        }
    }

    @ExceptionHandler({IOException.class, DataAccessException.class})
    public ResponseEntity<?> handleStorageFileNotFound(Exception e) {
        LOG.error("The system was not able to upload the file. Cause: {}", e.getMessage(), e);
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body("The system was not able to upload the file. Cause: " + e.getMessage());
    }
}
