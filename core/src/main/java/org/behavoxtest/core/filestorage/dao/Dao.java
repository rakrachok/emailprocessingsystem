package org.behavoxtest.core.filestorage.dao;

import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;

/**
 * Base persistence interface. Used mostly to perform standalone non-batch or streaming persistence operations
 * @param <Id> id of an entity
 * @param <Data> entity to persist
 */
public interface Dao<Id, Data> {
    void readTo(FileStorageEntityHolder<Id, Data> fileStorageEntityHolder) throws DataAccessException;

    void persist(FileStorageEntityHolder<Id, Data> fileStorageEntityHolder) throws DataAccessException;

    void delete(FileStorageEntityHolder<Id, Data> fileStorageEntityHolder) throws DataAccessException;
}
