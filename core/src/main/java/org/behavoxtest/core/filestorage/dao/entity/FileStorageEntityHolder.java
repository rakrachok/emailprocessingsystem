package org.behavoxtest.core.filestorage.dao.entity;

import java.io.Serializable;

/**
 * A general wrapper for the file storage entities like text, audio, any other binary files.
 */
public class FileStorageEntityHolder<IdType, EntityType> implements Serializable {

    private final IdType id;
    private final EntityType entity;

    public FileStorageEntityHolder(IdType id, EntityType entity) {
        this.id = id;
        this.entity = entity;
    }

    public EntityType getEntity() {
        return this.entity;
    }

    public IdType getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof FileStorageEntityHolder){
            FileStorageEntityHolder entityHolder = (FileStorageEntityHolder) obj;
            return id.equals(entityHolder.id) && entity.equals(entityHolder.entity);
        }

        return false;
    }
}
