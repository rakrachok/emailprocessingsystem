package org.behavoxtest.core.filestorage.dao.entity;

import java.io.Serializable;

/**
 * Interface which represents a general database entity holder
 */
public interface Persistable<T> extends Serializable {
    /**
     * Method returns a type that will be saved in the database
     */
    T toPersistableType();
}
