package org.behavoxtest.core.filestorage.datasource;

import java.io.Serializable;
import java.util.Properties;

/**
 * Primary interface of all data source implementations (HdfsDataSource.class, MySqlDataSource.class
 */
public interface DataSource extends Serializable {
    String getConnectionString();

    default Properties getConnectionProperties() {
        return new Properties();
    }
}
