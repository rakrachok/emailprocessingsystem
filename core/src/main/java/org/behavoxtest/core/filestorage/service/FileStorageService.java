package org.behavoxtest.core.filestorage.service;

import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;

import java.net.URI;

/**
 * Service interface to store data. works along with the interface Dao
 */
public interface FileStorageService {

    void persist(String locator, URI resource) throws DataAccessException;

    void delete(String locator, URI resource) throws DataAccessException;
}
