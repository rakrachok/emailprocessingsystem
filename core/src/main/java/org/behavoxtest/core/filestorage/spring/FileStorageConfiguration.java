package org.behavoxtest.core.filestorage.spring;

/**
 * General FileStorage config. Can be used in the future to autowire all FileStorage/Storage configs
 */
public interface FileStorageConfiguration {
}
