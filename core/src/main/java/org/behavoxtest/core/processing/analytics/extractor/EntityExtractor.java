package org.behavoxtest.core.processing.analytics.extractor;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Basic entity extractor which is used to extract artifacts, features.
 * @param <T> input
 * @param <R> output
 */
public interface EntityExtractor<T, R> extends Function<T, R>, Serializable {

}
