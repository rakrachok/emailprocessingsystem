package org.behavoxtest.core.processing.analytics.feature;

import java.io.Serializable;

/**
 * Primary feature class
 * @param <T> type of value
 */
public abstract class PrimaryFeature<T> implements Serializable {
    protected T value;

    /**
     * Used for deserialization purposes
     */
    public PrimaryFeature() {
    }

    public PrimaryFeature(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
