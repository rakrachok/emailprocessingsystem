package org.behavoxtest.core.processing.analytics.feature;

import java.io.Serializable;

/**
 * Secondary feature class
 * @param <T> type of value
 */
public abstract class SecondaryFeature<T> implements Serializable {
    private T value;

    public SecondaryFeature(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
