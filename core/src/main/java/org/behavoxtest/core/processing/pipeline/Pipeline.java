package org.behavoxtest.core.processing.pipeline;

import java.util.Queue;

/**
 * Pipeline is a runnable line or queue of different PipelineOperations.
 */
public interface Pipeline extends Runnable {
    Queue<PipelineOperation> getOperations();

    void addPipelineOperation(PipelineOperation operation);

    default void start() {
        getOperations().forEach(pipelineOperation -> pipelineOperation.apply(null));
    }

}
