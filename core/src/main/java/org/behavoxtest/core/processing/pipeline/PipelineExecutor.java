package org.behavoxtest.core.processing.pipeline;

/**
 * Executor class which manages how the pipeline is being executed
 */
public interface PipelineExecutor {
    static void execute(Pipeline pipeLine) {
        pipeLine.start();
    }
}
