package org.behavoxtest.core.processing.pipeline;

import java.util.function.Function;

/**
 * Primary pipeline operation
 * @param <T> input type
 * @param <R> output type
 */
public interface PipelineOperation<T, R> extends Function<T, R> {
}
