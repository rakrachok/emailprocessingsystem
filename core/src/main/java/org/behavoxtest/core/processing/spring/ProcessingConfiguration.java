package org.behavoxtest.core.processing.spring;

/**
 * General Processing config. Can be used in the future to autowire all Processing configs
 */
public interface ProcessingConfiguration {
}
