package org.behavoxtest.processing;

import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.behavoxtest.core.filestorage.datasource.DataSource;
import org.behavoxtest.core.processing.pipeline.Pipeline;
import org.behavoxtest.core.processing.pipeline.PipelineOperation;
import org.behavoxtest.core.processing.spring.ProcessingConfiguration;
import org.behavoxtest.processing.analytics.artifact.extractor.ArtifactExtractorGlobal;
import org.behavoxtest.processing.analytics.primary.extractor.PrimaryFeatureExtractorGlobal;
import org.behavoxtest.processing.analytics.secondary.extractor.SecondaryFeatureExtractorGlobal;
import org.behavoxtest.processing.pipeline.EmailProcessingPipeline;
import org.behavoxtest.processing.spark.pipelineoperation.ArtifactExtractionOperation;
import org.behavoxtest.processing.spark.pipelineoperation.PrimaryFeatureCalculationOperation;
import org.behavoxtest.processing.spark.pipelineoperation.SecondaryFeatureCalculationOperation;
import org.behavoxtest.storage.HdfsConfiguration;
import org.behavoxtest.storage.MySqlConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

/**
 * Config for processing beans
 */
@Configuration
@Import({SparkConfiguration.class, HdfsConfiguration.class, MySqlConfiguration.class})
@PropertySource("classpath:processing.properties")
@ComponentScan(basePackages = "org.behavoxtest.processing.*")
public class EmailProcessingConfiguration implements ProcessingConfiguration {

    /**
     * Time in milliseconds to wait until the last pipeline operation starts (Secondary feature calculation)
     */
    @Value("${emailprocessing.batch.duration}")
    private long batchDuration;

    @Bean
    public Pipeline emailProcessingPipeline(
            @Autowired JavaStreamingContext streamingContext,
            @Autowired ArtifactExtractorGlobal artifactExtractorGlobal,
            @Autowired PrimaryFeatureExtractorGlobal primaryFeatureExtractorGlobal,
            @Autowired SecondaryFeatureExtractorGlobal secondaryFeatureExtractorGlobal,
            @Autowired @Qualifier("incomingHdfsDataSource") DataSource incomingHdfsDataSource,
            @Autowired @Qualifier("primaryFeatureHdfsDataSource") DataSource primaryFeatureHdfsDataSource,
            @Autowired @Qualifier("secondaryFeatureHdfsDataSource") DataSource secondaryFeatureHdfsDataSource,
            @Autowired @Qualifier("mysqlDataSource") DataSource mysqlDataSource) {

        PipelineOperation artifactExtractionOperation = new ArtifactExtractionOperation(streamingContext,
            artifactExtractorGlobal, incomingHdfsDataSource, mysqlDataSource);

        PipelineOperation primaryFeatureCalculationOperation = new PrimaryFeatureCalculationOperation(streamingContext,
            primaryFeatureExtractorGlobal, mysqlDataSource, primaryFeatureHdfsDataSource);

        PipelineOperation secondaryFeatureCalculationOperation = new SecondaryFeatureCalculationOperation(streamingContext.sparkContext(),
            secondaryFeatureExtractorGlobal, primaryFeatureHdfsDataSource, secondaryFeatureHdfsDataSource, mysqlDataSource);

        Pipeline pipeline = new EmailProcessingPipeline(streamingContext, batchDuration);
        pipeline.addPipelineOperation(artifactExtractionOperation);
        pipeline.addPipelineOperation(primaryFeatureCalculationOperation);
        pipeline.addPipelineOperation(secondaryFeatureCalculationOperation);

        Thread threadedPipeline = new Thread(pipeline);
        threadedPipeline.start();

        return pipeline;
    }

}
