package org.behavoxtest.processing;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Config for Spark beans
 */
@Configuration
public class SparkConfiguration {

    @Bean
    public SparkConf sparkConf() {
        return new SparkConf()
            .setAppName("file-processing-system")
            .setMaster("local[*]");
    }

    @Bean
    public JavaStreamingContext streamingContext() {
        JavaStreamingContext streamingContext = new JavaStreamingContext(sparkConf(), Duration.apply(2000));
        streamingContext.sparkContext().hadoopConfiguration().addResource(new org.apache.hadoop.conf.Configuration(true));
        return streamingContext;
    }
}
