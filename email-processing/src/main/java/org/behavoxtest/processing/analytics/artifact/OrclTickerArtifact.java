package org.behavoxtest.processing.analytics.artifact;

import org.behavoxtest.processing.analytics.common.Email;

/**
 * ORCL artifact
 */
public class OrclTickerArtifact extends TickerArtifact {
    public OrclTickerArtifact(Email email, String ticker, int pos) {
        super(email, ticker, pos);
    }
}
