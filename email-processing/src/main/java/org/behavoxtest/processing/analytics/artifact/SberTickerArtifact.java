package org.behavoxtest.processing.analytics.artifact;

import org.behavoxtest.processing.analytics.common.Email;

/**
 * SBER artifact
 */
public class SberTickerArtifact extends TickerArtifact {
    public SberTickerArtifact(Email email, String ticker, int pos) {
        super(email, ticker, pos);
    }
}
