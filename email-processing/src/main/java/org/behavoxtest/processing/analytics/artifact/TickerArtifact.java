package org.behavoxtest.processing.analytics.artifact;

import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.storage.dao.mysql.entity.TickerArtifactDbEntity;

/**
 * General artifact for ORCL/SBER or any other artifacts which participate in relevance calculation
 */
public class TickerArtifact implements Persistable<TickerArtifactDbEntity> {

    public static final String TABLE_NAME = "artifact";
    public static final Class STRUCTURED_TYPE = TickerArtifactDbEntity.class;

    private Email email;
    private String ticker;
    private Integer pos;

    public TickerArtifact(Email email, String ticker, Integer pos) {
        this.email = email;
        this.ticker = ticker;
        this.pos = pos;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmailId(Email email) {
        this.email = email;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    @Override
    public TickerArtifactDbEntity toPersistableType() {
        return new TickerArtifactDbEntity(email.getEmailId(), ticker, pos);
    }
}
