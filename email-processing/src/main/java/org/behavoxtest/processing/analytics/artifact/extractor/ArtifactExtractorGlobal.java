package org.behavoxtest.processing.analytics.artifact.extractor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Holder of the full amount of artifact extractors. Spring injects all algorithm-based extractors to work with
 */
@Component
public class ArtifactExtractorGlobal {
    private final Collection<TickerArtifactExtractor> artifactExtractors;

    @Autowired
    public ArtifactExtractorGlobal(Collection<TickerArtifactExtractor> artifactExtractors) {
        this.artifactExtractors = artifactExtractors;
    }

    public Collection<TickerArtifactExtractor> getArtifactExtractors() {
        return artifactExtractors;
    }
}
