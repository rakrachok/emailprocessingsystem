package org.behavoxtest.processing.analytics.artifact.extractor;

import org.behavoxtest.processing.analytics.artifact.OrclTickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.springframework.stereotype.Component;

/**
 * Specific artifact extractor for ORCL
 */
@Component
public class OrclTickerArtifactExtractor extends TickerArtifactExtractor {
    private static final String TICKER = "ORCL";

    public OrclTickerArtifactExtractor() {
        super(TICKER);
    }

    @Override
    protected OrclTickerArtifact getTickerArtifact(Email email, String ticker, int pos) {
        return new OrclTickerArtifact(email, ticker, pos);
    }
}
