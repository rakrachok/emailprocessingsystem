package org.behavoxtest.processing.analytics.artifact.extractor;

import org.behavoxtest.processing.analytics.artifact.SberTickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.springframework.stereotype.Component;

/**
 * Specific artifact extractor for SBER
 */
@Component
public class SberTickerArtifactExtractor extends TickerArtifactExtractor {
    private static final String TICKER = "SBER";

    public SberTickerArtifactExtractor() {
        super(TICKER);
    }

    @Override
    protected SberTickerArtifact getTickerArtifact(Email email, String ticker, int pos) {
        return new SberTickerArtifact(email, ticker, pos);
    }
}
