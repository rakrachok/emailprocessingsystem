package org.behavoxtest.processing.analytics.artifact.extractor;

import org.behavoxtest.core.processing.analytics.extractor.EntityExtractor;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;

/**
 * General artifact executor to fetch ORCL/SBER or any other artifacts which participate in relevance calculation
 */
public abstract class TickerArtifactExtractor implements EntityExtractor<Email, TickerArtifact> {
    private final String ticker;

    public TickerArtifactExtractor(String ticker) {
        this.ticker = ticker;
    }

    public TickerArtifact apply(Email email) {
        int pos = email.getContent().indexOf(ticker);
        return pos < 0 ? null : getTickerArtifact(email, ticker, pos);
    }

    protected abstract TickerArtifact getTickerArtifact(Email email, String ticker, int pos);
}
