package org.behavoxtest.processing.analytics.common;


import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.storage.dao.mysql.entity.EmailDbEntity;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * This class represents an email
 */
public class Email implements Persistable<EmailDbEntity> {

    public final static String TABLE_NAME = "email";
    public final static Class STRUCTURED_TYPE = EmailDbEntity.class;

    private String emailId;

    private String sentDate;

    private String from;

    private List<String> to;

    private List<String> cc;

    private String subject;

    private String content;

    private Map<String, String> attachments;

    private Integer bodyLength;

    private URI emailUri;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getAttachments() {
        return attachments;
    }

    public void setAttachments(Map<String, String> attachments) {
        this.attachments = attachments;
    }

    public URI getEmailUri() {
        return emailUri;
    }

    public void setEmailUri(URI emailUri) {
        this.emailUri = emailUri;
    }

    public Integer getBodyLength() {
        return bodyLength;
    }

    public void setBodyLength(Integer bodyLength) {
        this.bodyLength = bodyLength;
    }

    @Override
    public EmailDbEntity toPersistableType() {
        return new EmailDbEntity(
            emailId,
            sentDate,
            from,
            String.join(",", to),
            String.join(",", cc),
            subject,
            content);
    }
}
