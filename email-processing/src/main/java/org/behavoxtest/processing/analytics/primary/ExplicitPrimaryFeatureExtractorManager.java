package org.behavoxtest.processing.analytics.primary;

import org.behavoxtest.processing.analytics.artifact.OrclTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.SberTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.primary.extractor.DateFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.EmailLengthFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.OrclRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.SberRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.feature.*;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class explicitly identifies a composite primary feature which contains all the primary features.
 * The functionality is used to be persisted into MySql databsae or any other relational database due to explicitely defined content
 */
public class ExplicitPrimaryFeatureExtractorManager {
    private DateFeatureExtractor dateFeatureExtractor;
    private EmailLengthFeatureExtractor emailLengthFeatureExtractor;
    private OrclRelevanceFeatureExtractor orclRelevanceFeatureExtractor;
    private SberRelevanceFeatureExtractor sberRelevanceFeatureExtractor;

    public ExplicitPrimaryFeatureExtractorManager() {
        this.dateFeatureExtractor = new DateFeatureExtractor();
        this.emailLengthFeatureExtractor = new EmailLengthFeatureExtractor();
        this.orclRelevanceFeatureExtractor = new OrclRelevanceFeatureExtractor();
        this.sberRelevanceFeatureExtractor = new SberRelevanceFeatureExtractor();
    }

    public ExplicitPrimaryFeatureExtractorManager(DateFeatureExtractor dateFeatureExtractor,
                                                  EmailLengthFeatureExtractor emailLengthFeatureExtractor,
                                                  OrclRelevanceFeatureExtractor orclRelevanceFeatureExtractor,
                                                  SberRelevanceFeatureExtractor sberRelevanceFeatureExtractor) {
        this.dateFeatureExtractor = dateFeatureExtractor;
        this.emailLengthFeatureExtractor = emailLengthFeatureExtractor;
        this.orclRelevanceFeatureExtractor = orclRelevanceFeatureExtractor;
        this.sberRelevanceFeatureExtractor = sberRelevanceFeatureExtractor;
    }

    private DateFeature computeDateFeature(Email email) {
        return dateFeatureExtractor.apply(email);
    }

    private EmailLengthFeature computeEmailLengthFeature(Email email) {
        return emailLengthFeatureExtractor.apply(email);
    }

    private OrclRelevanceFeature computeOrclRelevanceFeature(TickerArtifact tickerArtifact) {
        return orclRelevanceFeatureExtractor.apply(tickerArtifact);
    }

    private SberRelevanceFeature computeSberRelevanceFeature(TickerArtifact tickerArtifact) {
        return sberRelevanceFeatureExtractor.apply(tickerArtifact);
    }

    public CompositeExplicitPrimaryFeature computePrimaryFeature(Email email, Collection<TickerArtifact> tickerArtifact) {
        DateFeature dateFeature = computeDateFeature(email);
        EmailLengthFeature emailLengthFeature = computeEmailLengthFeature(email);

        Map<? extends Class<? extends TickerArtifact>, TickerArtifact> map = tickerArtifact.stream()
            .collect(Collectors.toMap(TickerArtifact::getClass, ta -> ta));

        OrclRelevanceFeature orclRelevanceFeature = computeOrclRelevanceFeature(map.get(OrclTickerArtifact.class));
        SberRelevanceFeature sberRelevanceFeature = computeSberRelevanceFeature(map.get(SberTickerArtifact.class));

        return new CompositeExplicitPrimaryFeature(email.getEmailId(), dateFeature, emailLengthFeature, orclRelevanceFeature, sberRelevanceFeature);
    }
}
