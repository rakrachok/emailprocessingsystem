package org.behavoxtest.processing.analytics.primary;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.primary.extractor.ArtifactRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.PrimaryFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositePrimaryFeature;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class identifies a composite primary feature which contains all dynamic primary features.
 * The type of features can be extanded by any plugin or module due to its dynamic nature
 * The functionality is used to be persisted into NoSql databsae or file system (HDFS, S3)
 */
public class PrimaryFeatureExtractorManager {
    private final Collection<PrimaryFeatureExtractor<Email, PrimaryFeature>> primaryFeatureExtractors;
    private final Collection<ArtifactRelevanceFeatureExtractor> relevanceFeatureExtractors;

    public PrimaryFeatureExtractorManager(Collection<PrimaryFeatureExtractor<Email, PrimaryFeature>> primaryFeatureExtractors,
                                          Collection<ArtifactRelevanceFeatureExtractor> relevanceFeatureExtractors) {
        this.primaryFeatureExtractors = primaryFeatureExtractors;
        this.relevanceFeatureExtractors = relevanceFeatureExtractors;
    }

    public CompositePrimaryFeature computePrimaryFeature(Email email, Collection<TickerArtifact> tickerArtifacts) {
        List<PrimaryFeature> primaryFeatures = primaryFeatureExtractors.stream()
            .map(pfe -> pfe.apply(email))
            .collect(Collectors.toList());

        List<ArtifactRelevanceFeature> artifactRelevanceFeatures = relevanceFeatureExtractors.stream()
            .flatMap(rfe -> tickerArtifacts.stream().map(rfe))
            .collect(Collectors.toList());

        return new CompositePrimaryFeature(email.getEmailId(), primaryFeatures, artifactRelevanceFeatures);
    }
}
