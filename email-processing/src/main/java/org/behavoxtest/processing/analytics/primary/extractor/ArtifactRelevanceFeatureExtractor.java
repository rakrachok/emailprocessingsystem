package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;

/**
 * Genetal artifact relevance extractor which is an ancestor for specific feature extractors like
 * OrclRelevanceFeatureExtractor.class
 * SberRelevanceFeatureExtractor.class
 */
public abstract class ArtifactRelevanceFeatureExtractor implements PrimaryFeatureExtractor<TickerArtifact, ArtifactRelevanceFeature> {
}
