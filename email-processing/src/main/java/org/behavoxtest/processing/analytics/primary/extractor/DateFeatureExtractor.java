package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * This extractor creates a date feature of an email
 */
@Component
public class DateFeatureExtractor extends GeneralInfoFeatureExtractor {

    @Override
    public DateFeature apply(Email email) {
        DateFeature dateFeature = new DateFeature(LocalDateTime.parse(email.getSentDate()));
        return dateFeature;
    }
}
