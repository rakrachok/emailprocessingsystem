package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;
import org.behavoxtest.processing.analytics.common.Email;
import org.springframework.stereotype.Component;

/**
 * This extractor creates a length numeric feature of an email
 */
@Component
public class EmailLengthFeatureExtractor extends GeneralInfoFeatureExtractor {
    @Override
    public EmailLengthFeature apply(Email email) {
        EmailLengthFeature emailLengthFeature = new EmailLengthFeature(email.getBodyLength());
        return emailLengthFeature;
    }
}
