package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.common.Email;

/**
 * This featore extractor is an ancestor for extractors like
 * DateFeatureExtractor.class
 * EmailLengthFeatureExtractor.class
 * or any other which perform computation on an email
 */
public abstract class GeneralInfoFeatureExtractor implements PrimaryFeatureExtractor<Email, PrimaryFeature> {
}
