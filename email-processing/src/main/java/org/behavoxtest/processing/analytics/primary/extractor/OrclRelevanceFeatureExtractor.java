package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.processing.analytics.artifact.OrclTickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.springframework.stereotype.Component;

/**
 * This extractor creates a calculates ORCL relevance
 */
@Component
public class OrclRelevanceFeatureExtractor extends ArtifactRelevanceFeatureExtractor {
    private static final Class ORCL = OrclTickerArtifact.class;

    @Override
    public OrclRelevanceFeature apply(TickerArtifact tickerArtifact) {
        if (tickerArtifact != null && ORCL.equals(tickerArtifact.getClass())) {
            Integer tickerPos = tickerArtifact.getPos();
            int length = tickerArtifact.getEmail().getContent().length();
            double relevance = (double) tickerPos / length;
            return new OrclRelevanceFeature(relevance);
        } else {
            return new OrclRelevanceFeature(null);
        }
    }
}
