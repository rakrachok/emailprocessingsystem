package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.extractor.EntityExtractor;

/**
 * General interface for primary feature computation
 * @param <T>
 * @param <R>
 */
public interface PrimaryFeatureExtractor<T, R> extends EntityExtractor<T, R> {
}
