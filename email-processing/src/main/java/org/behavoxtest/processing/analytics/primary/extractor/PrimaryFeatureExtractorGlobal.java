package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.common.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Primary feature extractors holder
 */
@Component
public class PrimaryFeatureExtractorGlobal {

    private final Collection<PrimaryFeatureExtractor<Email, PrimaryFeature>> primaryFeatureExtractors;
    private final Collection<ArtifactRelevanceFeatureExtractor> relevanceFeatureExtractors;

    @Autowired
    public PrimaryFeatureExtractorGlobal(Collection<PrimaryFeatureExtractor<Email, PrimaryFeature>> primaryFeatureExtractors,
                                         Collection<ArtifactRelevanceFeatureExtractor> relevanceFeatureExtractors) {
        this.primaryFeatureExtractors = primaryFeatureExtractors;
        this.relevanceFeatureExtractors = relevanceFeatureExtractors;
    }

    public Collection<PrimaryFeatureExtractor<Email, PrimaryFeature>> getPrimaryFeatureExtractors() {
        return primaryFeatureExtractors;
    }

    public Collection<ArtifactRelevanceFeatureExtractor> getRelevanceFeatureExtractors() {
        return relevanceFeatureExtractors;
    }
}
