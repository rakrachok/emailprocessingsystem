package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.processing.analytics.artifact.SberTickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.SberRelevanceFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.springframework.stereotype.Component;

/**
 * This extractor creates a calculates SBER relevance
 */
@Component
public class SberRelevanceFeatureExtractor extends ArtifactRelevanceFeatureExtractor {
    private static final Class SBER = SberTickerArtifact.class;

    @Override
    public SberRelevanceFeature apply(TickerArtifact tickerArtifact) {
        if (tickerArtifact != null && SBER.equals(tickerArtifact.getClass())) {
            int tickerPos = tickerArtifact.getPos();
            int length = tickerArtifact.getEmail().getContent().length();
            double relevance = (double) tickerPos / length;
            return new SberRelevanceFeature(relevance);
        } else {
            return new SberRelevanceFeature(null);
        }
    }
}
