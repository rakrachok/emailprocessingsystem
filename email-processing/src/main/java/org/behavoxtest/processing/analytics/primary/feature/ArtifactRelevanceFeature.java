package org.behavoxtest.processing.analytics.primary.feature;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;

public abstract class ArtifactRelevanceFeature extends PrimaryFeature<Double> {
    public ArtifactRelevanceFeature() {
    }

    public ArtifactRelevanceFeature(Double value) {
        super(value);
    }
}
