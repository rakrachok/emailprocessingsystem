package org.behavoxtest.processing.analytics.primary.feature;

import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.storage.dao.mysql.entity.PrimaryFeatureDbEntity;

public class CompositeExplicitPrimaryFeature implements Persistable {

    public final static String TABLE_NAME = "primary_feature";
    public final static Class STRUCTURED_TYPE = PrimaryFeatureDbEntity.class;

    private String emailId;
    private DateFeature dateFeature;
    private EmailLengthFeature emailLengthFeature;
    private OrclRelevanceFeature orclRelevance;
    private SberRelevanceFeature sberRelevance;

    public CompositeExplicitPrimaryFeature() {
    }

    public CompositeExplicitPrimaryFeature(String emailId, DateFeature dateFeature, EmailLengthFeature emailLengthFeature) {
        this.emailId = emailId;
        this.dateFeature = dateFeature;
        this.emailLengthFeature = emailLengthFeature;
    }

    public CompositeExplicitPrimaryFeature(String emailId, DateFeature dateFeature, EmailLengthFeature emailLengthFeature,
                                           OrclRelevanceFeature orclRelevance, SberRelevanceFeature sberRelevance) {
        this.emailId = emailId;
        this.dateFeature = dateFeature;
        this.emailLengthFeature = emailLengthFeature;
        this.orclRelevance = orclRelevance;
        this.sberRelevance = sberRelevance;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public DateFeature getDateFeature() {
        return dateFeature;
    }

    public void setDateFeature(DateFeature dateFeature) {
        this.dateFeature = dateFeature;
    }

    public EmailLengthFeature getEmailLengthFeature() {
        return emailLengthFeature;
    }

    public void setEmailLengthFeature(EmailLengthFeature emailLengthFeature) {
        this.emailLengthFeature = emailLengthFeature;
    }

    public OrclRelevanceFeature getOrclRelevance() {
        return orclRelevance;
    }

    public void setOrclRelevance(OrclRelevanceFeature orclRelevance) {
        this.orclRelevance = orclRelevance;
    }

    public SberRelevanceFeature getSberRelevance() {
        return sberRelevance;
    }

    public void setSberRelevance(SberRelevanceFeature sberRelevance) {
        this.sberRelevance = sberRelevance;
    }

    @Override
    public PrimaryFeatureDbEntity toPersistableType() {
        return new PrimaryFeatureDbEntity(emailId, dateFeature.getValue(),
            emailLengthFeature.getValue(), orclRelevance.getValue(), sberRelevance.getValue());
    }
}