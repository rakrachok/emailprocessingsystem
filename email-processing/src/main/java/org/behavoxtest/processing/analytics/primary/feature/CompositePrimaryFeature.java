package org.behavoxtest.processing.analytics.primary.feature;

import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CompositePrimaryFeature implements Persistable<Map<Class<? extends PrimaryFeature>, PrimaryFeature>> {

    private final String emailId;
    private final List<PrimaryFeature> primaryFeatures;
    private final List<ArtifactRelevanceFeature> artifactRelevanceFeatures;
    private Map<Class<? extends PrimaryFeature>, PrimaryFeature> allFeatures;

    public CompositePrimaryFeature(String emailId, List<PrimaryFeature> primaryFeatures, List<ArtifactRelevanceFeature> artifactRelevanceFeatures) {
        this.emailId = emailId;
        this.primaryFeatures = primaryFeatures;
        this.artifactRelevanceFeatures = artifactRelevanceFeatures;

        ArrayList<PrimaryFeature> allFeatures = new ArrayList<>(primaryFeatures);
        allFeatures.addAll(artifactRelevanceFeatures);
        this.allFeatures = allFeatures.stream()
            .filter(pf -> pf.getValue() != null)
            .collect(Collectors.toMap(PrimaryFeature::getClass, value -> value));
    }

    public String getEmailId() {
        return emailId;
    }

    public List<PrimaryFeature> getPrimaryFeatures() {
        return primaryFeatures;
    }

    public List<ArtifactRelevanceFeature> getArtifactRelevanceFeatures() {
        return artifactRelevanceFeatures;
    }

    @Override
    public Map<Class<? extends PrimaryFeature>, PrimaryFeature> toPersistableType() {
        return allFeatures;
    }

}