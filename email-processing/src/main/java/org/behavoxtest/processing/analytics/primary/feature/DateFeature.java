package org.behavoxtest.processing.analytics.primary.feature;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;

import java.time.LocalDateTime;

public class DateFeature extends PrimaryFeature<LocalDateTime> {
    public DateFeature() {
    }

    public DateFeature(LocalDateTime value) {
        super(value);
    }
}
