package org.behavoxtest.processing.analytics.primary.feature;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;

public class EmailLengthFeature extends PrimaryFeature<Integer> {
    public EmailLengthFeature() {
    }

    public EmailLengthFeature(Integer value) {
        super(value);
    }
}
