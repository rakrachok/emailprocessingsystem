package org.behavoxtest.processing.analytics.primary.feature;

public class OrclRelevanceFeature extends ArtifactRelevanceFeature {
    public OrclRelevanceFeature() {
    }

    public OrclRelevanceFeature(Double value) {
        super(value);
    }
}
