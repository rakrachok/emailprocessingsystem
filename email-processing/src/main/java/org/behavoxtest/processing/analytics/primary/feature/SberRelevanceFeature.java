package org.behavoxtest.processing.analytics.primary.feature;

public class SberRelevanceFeature extends ArtifactRelevanceFeature {
    public SberRelevanceFeature() {
    }

    public SberRelevanceFeature(Double value) {
        super(value);
    }
}
