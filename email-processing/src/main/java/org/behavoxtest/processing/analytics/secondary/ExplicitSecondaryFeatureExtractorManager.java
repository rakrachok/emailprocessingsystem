package org.behavoxtest.processing.analytics.secondary;

import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;
import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;
import org.behavoxtest.processing.analytics.secondary.extractor.AvgEmailLengthFeatureExtractor;
import org.behavoxtest.processing.analytics.secondary.extractor.FinanceCompanyRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.secondary.extractor.TechCompanyRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.secondary.extractor.UsualEmailTimeFeatureExtractor;
import org.behavoxtest.processing.analytics.secondary.feature.*;

import java.util.Collection;

/**
 * Non-dynamic, explicitly defined manager to work with CompositeExplicitSecondaryFeature.class
 */
public class ExplicitSecondaryFeatureExtractorManager {

    private final UsualEmailTimeFeatureExtractor usualEmailTimeFeatureExtractor;
    private final AvgEmailLengthFeatureExtractor avgEmailLengthFeatureExtractor;
    private final TechCompanyRelevanceFeatureExtractor techCompanyRelevanceFeatureExtractor;
    private final FinanceCompanyRelevanceFeatureExtractor financeCompanyRelevanceFeatureExtractor;

    public ExplicitSecondaryFeatureExtractorManager() {
        this.usualEmailTimeFeatureExtractor = new UsualEmailTimeFeatureExtractor();
        this.avgEmailLengthFeatureExtractor = new AvgEmailLengthFeatureExtractor();
        this.techCompanyRelevanceFeatureExtractor = new TechCompanyRelevanceFeatureExtractor();
        this.financeCompanyRelevanceFeatureExtractor = new FinanceCompanyRelevanceFeatureExtractor();
    }

    public ExplicitSecondaryFeatureExtractorManager(UsualEmailTimeFeatureExtractor usualEmailTimeFeatureExtractor,
                                                    AvgEmailLengthFeatureExtractor avgEmailLengthFeatureExtractor,
                                                    TechCompanyRelevanceFeatureExtractor techCompanyRelevanceFeatureExtractor,
                                                    FinanceCompanyRelevanceFeatureExtractor financeCompanyRelevanceFeatureExtractor) {
        this.usualEmailTimeFeatureExtractor = usualEmailTimeFeatureExtractor;
        this.avgEmailLengthFeatureExtractor = avgEmailLengthFeatureExtractor;
        this.techCompanyRelevanceFeatureExtractor = techCompanyRelevanceFeatureExtractor;
        this.financeCompanyRelevanceFeatureExtractor = financeCompanyRelevanceFeatureExtractor;
    }

    public UsualEmailTimeFeature computeUsualEmailTimeFeature(Collection<DateFeature> dateFeatures) {
        return usualEmailTimeFeatureExtractor.apply(dateFeatures);
    }

    public AvgEmailLengthFeature computeAvgEmailLengthFeature(Collection<EmailLengthFeature> emailLengthFeatures) {
        return avgEmailLengthFeatureExtractor.apply(emailLengthFeatures);
    }

    public TechCompanyRelevanceFeature computeTechCompanyRelevanceFeature(Collection<ArtifactRelevanceFeature> orclRelevanceFeatures) {
        return techCompanyRelevanceFeatureExtractor.apply(orclRelevanceFeatures);
    }

    public FinanceCompanyRelevanceFeature computeFinanceCompanyRelevanceFeature(Collection<ArtifactRelevanceFeature> sberRelevanceFeatures) {
        return financeCompanyRelevanceFeatureExtractor.apply(sberRelevanceFeatures);
    }

    public CompositeExplicitSecondaryFeature computeSecondaryFeature(Collection<DateFeature> dateFeatures,
                                                             Collection<EmailLengthFeature> emailLengthFeatures,
                                                             Collection<ArtifactRelevanceFeature> orclRelevanceFeatures,
                                                             Collection<ArtifactRelevanceFeature> sberRelevanceFeatures) {
        UsualEmailTimeFeature usualEmailTimeFeature = usualEmailTimeFeatureExtractor.apply(dateFeatures);
        AvgEmailLengthFeature avgEmailLengthFeature = avgEmailLengthFeatureExtractor.apply(emailLengthFeatures);
        TechCompanyRelevanceFeature techCompanyRelevanceFeature = techCompanyRelevanceFeatureExtractor.apply(orclRelevanceFeatures);
        FinanceCompanyRelevanceFeature financeCompanyRelevanceFeature = financeCompanyRelevanceFeatureExtractor.apply(sberRelevanceFeatures);

        return new CompositeExplicitSecondaryFeature(usualEmailTimeFeature, avgEmailLengthFeature, techCompanyRelevanceFeature, financeCompanyRelevanceFeature);
    }
}
