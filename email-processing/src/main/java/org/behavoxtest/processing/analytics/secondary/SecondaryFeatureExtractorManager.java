package org.behavoxtest.processing.analytics.secondary;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositePrimaryFeature;
import org.behavoxtest.processing.analytics.secondary.extractor.SecondaryFeatureExtractor;
import org.behavoxtest.processing.analytics.secondary.feature.CompositeSecondaryFeature;

import java.util.*;

/**
 * Base manager-extractor for secondary features which are implicitly defined (dynamic nature)
 */
public class SecondaryFeatureExtractorManager {
    private final Collection<SecondaryFeatureExtractor> secondaryFeatureExtractors;

    public SecondaryFeatureExtractorManager(Collection<SecondaryFeatureExtractor> secondaryFeatureExtractors) {
        this.secondaryFeatureExtractors = secondaryFeatureExtractors;
    }

    public CompositeSecondaryFeature computeSecondaryFeature(List<CompositePrimaryFeature> compositePrimaryFeatures) {
        List<SecondaryFeature> secondaryFeatures = new ArrayList<>();

        Map<Class, List<PrimaryFeature>> primaryFeatureMap = new HashMap<>();
        compositePrimaryFeatures.forEach(cpf -> {
            cpf.getPrimaryFeatures().forEach(pf -> {
                List<PrimaryFeature> list = primaryFeatureMap.get(pf.getClass());
                if (list == null) {
                    list = new LinkedList<>();
                }
                list.add(pf);
                primaryFeatureMap.put(pf.getClass(), list);
            });
            cpf.getArtifactRelevanceFeatures().forEach(pf -> {
                List<PrimaryFeature> list = primaryFeatureMap.get(pf.getClass());
                if (list == null) {
                    list = new LinkedList<>();
                }
                list.add(pf);
                primaryFeatureMap.put(pf.getClass(), list);
            });
        });

        secondaryFeatureExtractors.forEach(extractor -> {
            List<PrimaryFeature> primaryFeatures = primaryFeatureMap.get(extractor.getApplicableClass());
            if (primaryFeatures != null) {
                SecondaryFeature sf = (SecondaryFeature) extractor.apply(primaryFeatures);
                secondaryFeatures.add(sf);
            }
        });

        return new CompositeSecondaryFeature(secondaryFeatures);
    }
}
