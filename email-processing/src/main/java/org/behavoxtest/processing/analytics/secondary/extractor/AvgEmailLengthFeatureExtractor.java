package org.behavoxtest.processing.analytics.secondary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;
import org.behavoxtest.processing.analytics.secondary.feature.AvgEmailLengthFeature;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Extractor for avarege email length feature
 */
@Component
public class AvgEmailLengthFeatureExtractor implements SecondaryFeatureExtractor<Collection<EmailLengthFeature>, AvgEmailLengthFeature> {
    @Override
    public AvgEmailLengthFeature apply(Collection<EmailLengthFeature> emailLengthFeatures) {
        int sum = emailLengthFeatures.stream()
            .mapToInt(PrimaryFeature::getValue)
            .sum();

        double avg = (double) sum / emailLengthFeatures.size();
        return new AvgEmailLengthFeature(avg);
    }

    @Override
    public Class<? extends PrimaryFeature> getApplicableClass() {
        return EmailLengthFeature.class;
    }
}
