package org.behavoxtest.processing.analytics.secondary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.SberRelevanceFeature;
import org.behavoxtest.processing.analytics.secondary.feature.FinanceCompanyRelevanceFeature;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Extractor for fin. companies relevance computation
 */
@Component
public class FinanceCompanyRelevanceFeatureExtractor implements SecondaryFeatureExtractor<Collection<ArtifactRelevanceFeature>, FinanceCompanyRelevanceFeature> {
    private final List<String> finCompanies = Arrays.asList("SBER");

    @Override
    public FinanceCompanyRelevanceFeature apply(Collection<ArtifactRelevanceFeature> artifactRelevanceFeatures) {
        List<Double> collect = artifactRelevanceFeatures.stream()
            .map(PrimaryFeature::getValue)
            .collect(Collectors.toList());

        Double relevance = 0d;
        if (collect.size() == 1) {
            relevance = collect.get(0);
        } else if (collect.size() > 1) {
            relevance = collect.stream()
                .filter(Objects::nonNull)
                .reduce((a1, a2) -> a1 + a2).get();
        }

        return new FinanceCompanyRelevanceFeature(relevance);
    }

    @Override
    public Class<? extends PrimaryFeature> getApplicableClass() {
        return SberRelevanceFeature.class;
    }
}
