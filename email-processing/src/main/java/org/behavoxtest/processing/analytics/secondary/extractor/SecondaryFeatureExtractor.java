package org.behavoxtest.processing.analytics.secondary.extractor;

import org.behavoxtest.core.processing.analytics.extractor.EntityExtractor;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;

/**
 * Base interface for secondary feature extractors
 * @param <T> input type
 * @param <R> output type
 */
public interface SecondaryFeatureExtractor<T, R> extends EntityExtractor<T, R> {
    Class<?extends PrimaryFeature> getApplicableClass();
}
