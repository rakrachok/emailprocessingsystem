package org.behavoxtest.processing.analytics.secondary.extractor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Container for all secondary features algorithm-based extractors wich are autowired from the classpath
 */
@Component
public class SecondaryFeatureExtractorGlobal {

    private final Collection<SecondaryFeatureExtractor> secondaryFeatureExtractors;

    @Autowired
    public SecondaryFeatureExtractorGlobal(Collection<SecondaryFeatureExtractor> secondaryFeatureExtractors) {
        this.secondaryFeatureExtractors = secondaryFeatureExtractors;
    }

    public Collection<SecondaryFeatureExtractor> getSecondaryFeatureExtractors() {
        return secondaryFeatureExtractors;
    }
}
