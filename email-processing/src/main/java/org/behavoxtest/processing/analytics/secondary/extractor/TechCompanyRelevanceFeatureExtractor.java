package org.behavoxtest.processing.analytics.secondary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;
import org.behavoxtest.processing.analytics.secondary.feature.TechCompanyRelevanceFeature;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Extractor which is used to calculate tech. company's relevance.
 */
@Component
public class TechCompanyRelevanceFeatureExtractor implements SecondaryFeatureExtractor<Collection<ArtifactRelevanceFeature>, TechCompanyRelevanceFeature> {
    private final Set<String> techCompanies = new HashSet<>(Arrays.asList("ORCL"));

    /**
     * Calculates tech companies relevance feature among the different artifact relevance features
     * @param artifactRelevanceFeatures collection of all technical artifacts found in a batch of emails
     * @return single technical relevance
     */
    @Override
    public TechCompanyRelevanceFeature apply(Collection<ArtifactRelevanceFeature> artifactRelevanceFeatures) {
        List<Double> collect = artifactRelevanceFeatures.stream()
            .map(ArtifactRelevanceFeature::getValue)
            .collect(Collectors.toList());

        Double relevance = 0d;
        if (collect.size() == 1) {
            relevance = collect.get(0);
        } else if (collect.size() > 1) {
            relevance = collect.stream()
                .filter(Objects::nonNull)
                .reduce((a1, a2) -> a1 + a2).get();
        }

        return new TechCompanyRelevanceFeature(relevance);
    }

    @Override
    public Class<? extends PrimaryFeature> getApplicableClass() {
        return OrclRelevanceFeature.class;
    }
}
