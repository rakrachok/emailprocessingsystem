package org.behavoxtest.processing.analytics.secondary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;
import org.behavoxtest.processing.analytics.secondary.feature.UsualEmailTimeFeature;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The class is used to computes usual emails time of the batch window
 */
@Component
public class UsualEmailTimeFeatureExtractor implements SecondaryFeatureExtractor<Collection<DateFeature>, UsualEmailTimeFeature> {

    private final String delimiter = "-";
    private final DateTimeFormatter dtfMin = DateTimeFormatter.ofPattern("h:m");
    private final DateTimeFormatter dtfMax = DateTimeFormatter.ofPattern("h:m a");

    /**
     * Calculates usual email time received
     * @param dateFeatures collection date primary features
     * @return
     */
    @Override
    public UsualEmailTimeFeature apply(Collection<DateFeature> dateFeatures) {
        List<LocalTime> localTimes = dateFeatures.stream()
            .map(dateFeature -> dateFeature.getValue().toLocalTime())
            .sorted()
            .collect(Collectors.toList());

        Optional<LocalTime> min = localTimes.stream().findFirst();
        Optional<LocalTime> max = localTimes.stream().reduce((v1, v2) -> v2);

        String usualTime = min.map(lt -> lt.format(dtfMin))
            .orElse("tbd") + delimiter + max.map(lt -> lt.format(dtfMax)).orElse("tbd");
        return new UsualEmailTimeFeature(usualTime);
    }

    @Override
    public Class<? extends PrimaryFeature> getApplicableClass() {
        return DateFeature.class;
    }
}
