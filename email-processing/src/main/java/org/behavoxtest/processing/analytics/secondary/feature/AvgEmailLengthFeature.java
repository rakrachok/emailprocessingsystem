package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;

/**
 * Average email length secondary feature
 */
public class AvgEmailLengthFeature extends SecondaryFeature<Double> {
    public AvgEmailLengthFeature(Double value) {
        super(value);
    }
}
