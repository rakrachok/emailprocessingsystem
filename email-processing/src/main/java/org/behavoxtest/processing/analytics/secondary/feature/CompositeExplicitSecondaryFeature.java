package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.storage.dao.mysql.entity.SecondaryFeatureDbEntity;

/**
 * The secondary feature classre presents explicitly defined amount of secondary features to be stored in the SQL storage
 */
public class CompositeExplicitSecondaryFeature implements Persistable<SecondaryFeatureDbEntity> {
    public static final String TABLE_NAME = "secondary_feature";
    public static final Class STRUCTURED_TYPE = SecondaryFeatureDbEntity.class;

    private UsualEmailTimeFeature usualEmailTime;
    private AvgEmailLengthFeature avgEmailLength;
    private TechCompanyRelevanceFeature techCompanyRelevanceFeature;
    private FinanceCompanyRelevanceFeature financeCompanyRelevanceFeature;

    public CompositeExplicitSecondaryFeature(UsualEmailTimeFeature usualEmailTime,
                                             AvgEmailLengthFeature avgEmailLength,
                                             TechCompanyRelevanceFeature techCompanyRelevanceFeature,
                                             FinanceCompanyRelevanceFeature financeCompanyRelevanceFeature) {
        this.usualEmailTime = usualEmailTime;
        this.avgEmailLength = avgEmailLength;
        this.techCompanyRelevanceFeature = techCompanyRelevanceFeature;
        this.financeCompanyRelevanceFeature = financeCompanyRelevanceFeature;
    }

    public UsualEmailTimeFeature getUsualEmailTime() {
        return usualEmailTime;
    }

    public void setUsualEmailTime(UsualEmailTimeFeature usualEmailTime) {
        this.usualEmailTime = usualEmailTime;
    }

    public AvgEmailLengthFeature getAvgEmailLength() {
        return avgEmailLength;
    }

    public void setAvgEmailLength(AvgEmailLengthFeature avgEmailLength) {
        this.avgEmailLength = avgEmailLength;
    }

    public TechCompanyRelevanceFeature getTechCompanyRelevanceFeature() {
        return techCompanyRelevanceFeature;
    }

    public void setTechCompanyRelevanceFeature(TechCompanyRelevanceFeature techCompanyRelevanceFeature) {
        this.techCompanyRelevanceFeature = techCompanyRelevanceFeature;
    }

    public FinanceCompanyRelevanceFeature getFinanceCompanyRelevanceFeature() {
        return financeCompanyRelevanceFeature;
    }

    public void setFinanceCompanyRelevanceFeature(FinanceCompanyRelevanceFeature financeCompanyRelevanceFeature) {
        this.financeCompanyRelevanceFeature = financeCompanyRelevanceFeature;
    }

    @Override
    public SecondaryFeatureDbEntity toPersistableType() {
        return new SecondaryFeatureDbEntity(usualEmailTime.getValue(), avgEmailLength.getValue(),
            techCompanyRelevanceFeature.getValue(), financeCompanyRelevanceFeature.getValue());
    }
}