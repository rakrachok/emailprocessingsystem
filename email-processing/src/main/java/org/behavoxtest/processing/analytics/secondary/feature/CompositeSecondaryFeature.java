package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.filestorage.dao.entity.Persistable;
import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Dynamic container of secondary features. Can be extended in collaboration with other dynamic containers like
 * CompositeSecondaryFeature.class + extractors and managers
 */
public class CompositeSecondaryFeature implements Persistable<Map<Class<? extends SecondaryFeature>, SecondaryFeature>> {

    private final List<SecondaryFeature> secondaryFeatures;

    public CompositeSecondaryFeature(List<SecondaryFeature> secondaryFeatures) {
        this.secondaryFeatures = secondaryFeatures;
    }

    public List<SecondaryFeature> getSecondaryFeatures() {
        return secondaryFeatures;
    }

    @Override
    public Map<Class<? extends SecondaryFeature>, SecondaryFeature> toPersistableType() {
        return secondaryFeatures.stream().collect(Collectors.toMap(sf -> sf.getClass(), sf -> sf));
    }
}