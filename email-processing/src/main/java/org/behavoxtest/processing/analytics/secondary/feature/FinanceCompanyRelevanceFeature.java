package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;

/**
 * Class represents financial relevance feature (secondary)
 */
public class FinanceCompanyRelevanceFeature extends SecondaryFeature<Double> {
    public FinanceCompanyRelevanceFeature(Double value) {
        super(value);
    }
}
