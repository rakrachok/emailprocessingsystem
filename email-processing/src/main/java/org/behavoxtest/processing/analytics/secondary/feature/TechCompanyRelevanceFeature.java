package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;

/**
 * Technical companies relevance feature (secondary)
 */
public class TechCompanyRelevanceFeature extends SecondaryFeature<Double> {
    public TechCompanyRelevanceFeature(Double value) {
        super(value);
    }
}
