package org.behavoxtest.processing.analytics.secondary.feature;

import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;

/**
 * Usual email time (secondary feature)
 */
public class UsualEmailTimeFeature extends SecondaryFeature<String> {
    public UsualEmailTimeFeature(String value) {
        super(value);
    }
}
