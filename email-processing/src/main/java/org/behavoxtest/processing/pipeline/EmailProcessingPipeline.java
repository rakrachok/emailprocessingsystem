package org.behavoxtest.processing.pipeline;

import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.behavoxtest.core.processing.pipeline.Pipeline;
import org.behavoxtest.core.processing.pipeline.PipelineOperation;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * This is the EmailProcessingPipeline. The pipeline which executes all
 */
public class EmailProcessingPipeline implements Pipeline {

    private JavaStreamingContext streamingContext;
    private long batchDuration;

    public EmailProcessingPipeline(JavaStreamingContext streamingContext, long batchDuration) {
        this.streamingContext = streamingContext;
        this.batchDuration = batchDuration;
    }

    private Queue<PipelineOperation> operations = new LinkedList<>();

    @Override
    public Queue<PipelineOperation> getOperations() {
        return operations;
    }

    @Override
    public void addPipelineOperation(PipelineOperation operation) {
        operations.add(operation);
    }

    @Override
    public void start() {
        Iterator<PipelineOperation> iterator = operations.iterator();
        PipelineOperation last = null;
        while(iterator.hasNext()) {
            PipelineOperation current = iterator.next();
            if (iterator.hasNext()) {
                PipelineOperation next = iterator.next();
                current.andThen(next).apply(null);
            } else {
                last = current;
            }
        }

        streamingContext.start();
        try {
            streamingContext.awaitTerminationOrTimeout(batchDuration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        last.apply(null);
    }

    @Override
    public void run() {
        start();
    }
}
