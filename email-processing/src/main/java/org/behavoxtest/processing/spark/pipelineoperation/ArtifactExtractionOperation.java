package org.behavoxtest.processing.spark.pipelineoperation;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.input.WholeTextFileInputFormat;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.behavoxtest.core.filestorage.datasource.DataSource;
import org.behavoxtest.core.processing.pipeline.PipelineOperation;
import org.behavoxtest.processing.analytics.artifact.extractor.ArtifactExtractorGlobal;
import org.behavoxtest.processing.analytics.artifact.extractor.TickerArtifactExtractor;
import org.behavoxtest.processing.util.EmailParser;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Artifact pipeline operation which is used to find and store the artifacts from emails
 */
public class ArtifactExtractionOperation implements PipelineOperation<Void, JavaPairDStream<Email, Collection<TickerArtifact>>> {

    private final static Logger LOG = LoggerFactory.getLogger(ArtifactExtractionOperation.class);

    private final JavaStreamingContext streamingContext;

    private ArtifactExtractorGlobal artifactExtractorGlobal;

    private final DataSource streamFrom;

    private final DataSource streamTo;

    public ArtifactExtractionOperation(JavaStreamingContext streamingContext,
           ArtifactExtractorGlobal artifactExtractorGlobal,
           DataSource streamFrom, DataSource streamTo) {
        this.streamingContext = streamingContext;
        this.artifactExtractorGlobal = artifactExtractorGlobal;
        this.streamFrom = streamFrom;
        this.streamTo = streamTo;
    }

    @Override
    public JavaPairDStream<Email, Collection<TickerArtifact>> apply(Void aVoid) {
        Set<TickerArtifactExtractor> artifactExtractors = new HashSet<>(artifactExtractorGlobal.getArtifactExtractors());
        streamingContext.sparkContext().broadcast(artifactExtractors);

        JavaPairInputDStream<Text, Text> javaPairDStream = streamingContext.fileStream(streamFrom.getConnectionString(),
            Text.class, Text.class, WholeTextFileInputFormat.class, (Function<Path, Boolean>) path -> true, false);

        JavaDStream<Email> emailDStream = javaPairDStream
            .mapToPair(t2 -> {
                LOG.debug("Mapping to pair from Hadoop to Java: [{}]", t2._1().toString());
                return new Tuple2<>(t2._1().toString(), t2._2().toString());
            })
            .map(t2 -> {
                LOG.debug("Mapping String to Java Input Stream to be able to activate the Parser: [{}]", t2._1());
                return EmailParser.parse(t2._1(), t2._2());
            });

        emailDStream.persist();

        emailDStream
            .map(Email::toPersistableType)
            .foreachRDD(emailDbEntityRdd -> {
                if (!emailDbEntityRdd.isEmpty()) {
                    LOG.debug("Persisting emailDbEntityRdd: {}", emailDbEntityRdd.toDebugString());
                    SQLContext sqlContext = SQLContext.getOrCreate(SparkContext.getOrCreate());
                    Dataset<Row> emailDataFrame = sqlContext.createDataFrame(emailDbEntityRdd, Email.STRUCTURED_TYPE);
                    emailDataFrame.write()
                        .mode(SaveMode.Append)
                        .jdbc(streamTo.getConnectionString(), Email.TABLE_NAME, streamTo.getConnectionProperties());
                    LOG.debug("Persisted: {}", emailDbEntityRdd.toDebugString());
                } else {
                    LOG.debug("emailDStream.foreachRDD: Nothing to read");
                }
        });

        JavaPairDStream<Email, Collection<TickerArtifact>> emailArtifactsDStream = emailDStream
            .mapToPair(email -> {
                LOG.debug("Extracting artifacts from email: {}", email);
                Stream<TickerArtifact> artifactsStream = artifactExtractors.stream()
                    .map(extr -> extr.apply(email))
                    .filter(Objects::nonNull);
                LOG.debug("Extracting has been finished");
                return new Tuple2<>(email, artifactsStream.collect(Collectors.toList()));
            });

        emailArtifactsDStream
            .flatMap(t2 -> t2._2().stream().map(TickerArtifact::toPersistableType).iterator())
            .foreachRDD(artifactDbEntityRdd -> {
                if (!artifactDbEntityRdd.isEmpty()) {
                    LOG.debug("Persisting artifactDbEntityRdd: {}", artifactDbEntityRdd.toDebugString());
                    SQLContext sqlContext = SQLContext.getOrCreate(SparkContext.getOrCreate());
                    Dataset<Row> dataFrame = sqlContext.createDataFrame(artifactDbEntityRdd, TickerArtifact.STRUCTURED_TYPE);
                    dataFrame.write()
                        .mode(SaveMode.Append)
                        .jdbc(streamTo.getConnectionString(), TickerArtifact.TABLE_NAME, streamTo.getConnectionProperties());
                    LOG.debug("Persisted: {}", artifactDbEntityRdd.toDebugString());
                } else {
                    LOG.debug("artifactDStream.foreachRDD: Nothing to read");
                }
        });

        return emailArtifactsDStream;
    }
}