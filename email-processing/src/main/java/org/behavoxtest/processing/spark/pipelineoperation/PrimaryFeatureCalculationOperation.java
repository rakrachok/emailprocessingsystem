package org.behavoxtest.processing.spark.pipelineoperation;

import org.apache.spark.SparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.behavoxtest.core.filestorage.datasource.DataSource;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.core.processing.pipeline.PipelineOperation;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.behavoxtest.processing.analytics.primary.ExplicitPrimaryFeatureExtractorManager;
import org.behavoxtest.processing.analytics.primary.PrimaryFeatureExtractorManager;
import org.behavoxtest.processing.analytics.primary.extractor.ArtifactRelevanceFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.PrimaryFeatureExtractor;
import org.behavoxtest.processing.analytics.primary.extractor.PrimaryFeatureExtractorGlobal;
import org.behavoxtest.processing.analytics.primary.feature.CompositeExplicitPrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositePrimaryFeature;
import org.behavoxtest.processing.util.CompositePrimaryFeatureJsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This pipeline operation calculates and stores primary features
 */
public class PrimaryFeatureCalculationOperation implements PipelineOperation<JavaPairDStream<Email, Collection<TickerArtifact>>, JavaDStream<CompositeExplicitPrimaryFeature>> {
    private final static Logger LOG = LoggerFactory.getLogger(PrimaryFeatureCalculationOperation.class);

    private JavaStreamingContext streamingContext;
    private final PrimaryFeatureExtractorGlobal primaryFeatureExtractorGlobal;
    private final DataSource streamToDb;
    private final DataSource streamToHdfs;

    public PrimaryFeatureCalculationOperation(JavaStreamingContext streamingContext,
                                              PrimaryFeatureExtractorGlobal primaryFeatureExtractorGlobal,
                                              DataSource streamToDb,
                                              DataSource streamToHdfs) {
        this.streamingContext = streamingContext;
        this.primaryFeatureExtractorGlobal = primaryFeatureExtractorGlobal;
        this.streamToDb = streamToDb;
        this.streamToHdfs = streamToHdfs;
    }

    @Override
    public JavaDStream<CompositeExplicitPrimaryFeature> apply(JavaPairDStream<Email, Collection<TickerArtifact>> emailArtifactsDStream) {
        Set<PrimaryFeatureExtractor<Email, PrimaryFeature>> primaryFeatureExtractors = new HashSet<>(primaryFeatureExtractorGlobal.getPrimaryFeatureExtractors());
        streamingContext.sparkContext().broadcast(primaryFeatureExtractors);

        Set<ArtifactRelevanceFeatureExtractor> relevanceFeatureExtractors = new HashSet<>(primaryFeatureExtractorGlobal.getRelevanceFeatureExtractors());
        streamingContext.sparkContext().broadcast(relevanceFeatureExtractors);

        CompositePrimaryFeatureJsonParser compositePrimaryFeatureJsonParser = new CompositePrimaryFeatureJsonParser();
        streamingContext.sparkContext().broadcast(compositePrimaryFeatureJsonParser);

        emailArtifactsDStream.persist();

        JavaDStream<CompositePrimaryFeature> primaryFeatureDStream = emailArtifactsDStream
            .map(t2 -> {
                LOG.debug("Calculating general composite primary feature for {}", t2.toString());
                Email email = t2._1();
                Collection<TickerArtifact> tickerArtifacts = t2._2();
                PrimaryFeatureExtractorManager primaryFeatureManager = new PrimaryFeatureExtractorManager(primaryFeatureExtractors, relevanceFeatureExtractors);
                CompositePrimaryFeature compositePrimaryFeature = primaryFeatureManager.computePrimaryFeature(email, tickerArtifacts);
                LOG.debug("Calculated: {}", compositePrimaryFeature);
                return compositePrimaryFeature;
            });

        JavaDStream<CompositeExplicitPrimaryFeature> explicitPrimaryFeatureDStream = emailArtifactsDStream
            .map(t2 -> {
                LOG.debug("Calculating explicit composite primary feature for {}", t2.toString());
                Email email = t2._1();
                Collection<TickerArtifact> tickerArtifacts = t2._2();
                ExplicitPrimaryFeatureExtractorManager explicitPrimaryFeatureManager = new ExplicitPrimaryFeatureExtractorManager();
                CompositeExplicitPrimaryFeature explicitPrimaryFeature = explicitPrimaryFeatureManager.computePrimaryFeature(email, tickerArtifacts);
                LOG.debug("Calculated: {}", explicitPrimaryFeature.toString());
                return explicitPrimaryFeature;
            });

        primaryFeatureDStream
            .map(compositePrimaryFeatureJsonParser::toJson)
            .foreachRDD(primaryFeatureRdd -> {
                if (!primaryFeatureRdd.isEmpty()) {
                    LOG.debug("Persisting general primary features to [{}]: {}", streamToHdfs.getConnectionString(), primaryFeatureRdd.toDebugString());
                    primaryFeatureRdd.saveAsTextFile(streamToHdfs.getConnectionString());
                    LOG.debug("Persisted: {}", primaryFeatureRdd.toDebugString());
                } else {
                    LOG.debug("primaryFeatureDStream.foreachRDD: Nothing to read");
                }
            });

        explicitPrimaryFeatureDStream
            .map(CompositeExplicitPrimaryFeature::toPersistableType)
            .foreachRDD(primaryFeatureRdd -> {
                if (!primaryFeatureRdd.isEmpty()) {
                    LOG.debug("Persisting explicit primary features to [{}]: {}", streamToDb.getConnectionString(), primaryFeatureRdd.toDebugString());
                    SQLContext sqlContext = SQLContext.getOrCreate(SparkContext.getOrCreate());
                    Dataset<Row> dataFrame = sqlContext.createDataFrame(primaryFeatureRdd, CompositeExplicitPrimaryFeature.STRUCTURED_TYPE);
                    dataFrame.write()
                        .mode(SaveMode.Append)
                        .jdbc(streamToDb.getConnectionString(), CompositeExplicitPrimaryFeature.TABLE_NAME, streamToDb.getConnectionProperties());

                    LOG.debug("Persisted: {}", primaryFeatureRdd.toDebugString());
                } else {
                    LOG.debug("primaryFeatureDStream.foreachRDD: Nothing to read");
                }
            });

        return explicitPrimaryFeatureDStream;
    }
}
