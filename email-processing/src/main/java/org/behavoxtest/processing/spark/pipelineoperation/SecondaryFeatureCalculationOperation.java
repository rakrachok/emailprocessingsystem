package org.behavoxtest.processing.spark.pipelineoperation;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.storage.StorageLevel;
import org.behavoxtest.core.filestorage.datasource.DataSource;
import org.behavoxtest.core.processing.pipeline.PipelineOperation;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositeExplicitPrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositePrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;
import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.SberRelevanceFeature;
import org.behavoxtest.processing.analytics.secondary.ExplicitSecondaryFeatureExtractorManager;
import org.behavoxtest.processing.analytics.secondary.SecondaryFeatureExtractorManager;
import org.behavoxtest.processing.analytics.secondary.extractor.SecondaryFeatureExtractorGlobal;
import org.behavoxtest.processing.analytics.secondary.feature.AvgEmailLengthFeature;
import org.behavoxtest.processing.analytics.secondary.feature.CompositeExplicitSecondaryFeature;
import org.behavoxtest.processing.analytics.secondary.feature.CompositeSecondaryFeature;
import org.behavoxtest.processing.analytics.secondary.feature.FinanceCompanyRelevanceFeature;
import org.behavoxtest.processing.analytics.secondary.feature.TechCompanyRelevanceFeature;
import org.behavoxtest.processing.analytics.secondary.feature.UsualEmailTimeFeature;
import org.behavoxtest.processing.util.CompositePrimaryFeatureJsonParser;
import org.behavoxtest.processing.util.CompositeSecondaryFeatureJsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Non-streaming operation which is reposible for calculating and persisting secondary features.
 * This is the last operation in the email pipeline
 */
public class SecondaryFeatureCalculationOperation implements PipelineOperation<Void, Void> {

    private final static Logger LOG = LoggerFactory.getLogger(SecondaryFeatureCalculationOperation.class);

    private final JavaSparkContext javaSparkContext;
    private SecondaryFeatureExtractorGlobal secondaryFeatureExtractorGlobal;
    private final DataSource streamFrom;
    private final DataSource streamToHdfs;
    private final DataSource mySqlDataSource;

    public SecondaryFeatureCalculationOperation(JavaSparkContext javaSparkContext,
                                                SecondaryFeatureExtractorGlobal secondaryFeatureExtractorGlobal,
                                                DataSource streamFrom, DataSource streamToHdfs, DataSource mySqlDataSource) {
        this.javaSparkContext = javaSparkContext;
        this.secondaryFeatureExtractorGlobal = secondaryFeatureExtractorGlobal;
        this.streamFrom = streamFrom;
        this.streamToHdfs = streamToHdfs;
        this.mySqlDataSource = mySqlDataSource;
    }

    @Override
    public Void apply(Void nothing) {
        SQLContext sqlContext = SQLContext.getOrCreate(javaSparkContext.sc());

        CompositePrimaryFeatureJsonParser compositePrimaryFeatureJsonParser = new CompositePrimaryFeatureJsonParser();
        javaSparkContext.broadcast(compositePrimaryFeatureJsonParser);

        JavaRDD<CompositePrimaryFeature> cpfRdd = javaSparkContext.wholeTextFiles(streamFrom.getConnectionString())
                .map(json -> compositePrimaryFeatureJsonParser.toObject(json._2()));

        JavaRDD<Row> rowRdd = sqlContext.read().jdbc(mySqlDataSource.getConnectionString(),
                CompositeExplicitPrimaryFeature.TABLE_NAME, mySqlDataSource.getConnectionProperties()).toJavaRDD();
        rowRdd.persist(StorageLevel.MEMORY_ONLY());

        JavaRDD<CompositeExplicitPrimaryFeature> cepfRdd = rowRdd.map(row -> {
            String emailId = row.getString(0);
            DateFeature dateFeature = new DateFeature(LocalDateTime.parse(row.getString(1)));
            EmailLengthFeature emailLengthFeature = new EmailLengthFeature(row.getInt(2));

            Object object = row.get(3);
            OrclRelevanceFeature orclRelevanceFeature;
            if (object == null) {
                orclRelevanceFeature = new OrclRelevanceFeature();
            } else {
                orclRelevanceFeature = new OrclRelevanceFeature(row.getDouble(3));
            }

            SberRelevanceFeature sberRelevanceFeature;
            object = row.get(4);
            if (object == null) {
                sberRelevanceFeature = new SberRelevanceFeature();
            } else {
                sberRelevanceFeature = new SberRelevanceFeature(row.getDouble(4));
            }

            return new CompositeExplicitPrimaryFeature(emailId, dateFeature, emailLengthFeature, orclRelevanceFeature, sberRelevanceFeature);
        });

        createAndStoreFeature(javaSparkContext, cpfRdd);
        createAndStoreExplicitFeature(sqlContext, cepfRdd);

        return null;
    }

    private void createAndStoreFeature(JavaSparkContext javaSparkContext, JavaRDD<CompositePrimaryFeature> cpfRdd) {
        if(cpfRdd.isEmpty()) {
            return;
        }

        LOG.debug("Reading and computing a general secondary feature");

        SecondaryFeatureExtractorManager implicitManager =
                new SecondaryFeatureExtractorManager(secondaryFeatureExtractorGlobal.getSecondaryFeatureExtractors());

        CompositeSecondaryFeature compositeSecondaryFeature = implicitManager.computeSecondaryFeature(cpfRdd.collect());
        CompositeSecondaryFeatureJsonParser compositeSecondaryFeatureJsonParser = new CompositeSecondaryFeatureJsonParser();

        String json;
        try {
            json = compositeSecondaryFeatureJsonParser.toJson(compositeSecondaryFeature);
        } catch (JsonProcessingException e) {
            LOG.warn("Failed to convert to JSON a general composite secondary feature.", e);
            throw new RuntimeException("Failed to convert to JSON a general composite secondary feature.", e);
        }

        JavaRDD<String> rdd = javaSparkContext.parallelize(Collections.singletonList(json))
            .repartition(1);

        LOG.debug("General secondary feature is calculated: {}", json);

        LOG.debug("Persisting general secondary feature as text to {}", streamToHdfs.getConnectionString());
        String destination = streamToHdfs.getConnectionString() + "/" + Instant.now().toEpochMilli();
        rdd.saveAsTextFile(destination);
        LOG.debug("Persisted as {}", destination);
    }

    private void createAndStoreExplicitFeature(SQLContext sqlContext, JavaRDD<CompositeExplicitPrimaryFeature> cepfRdd) {
        if(cepfRdd.isEmpty()) {
            return;
        }

        LOG.debug("Reading and computing an explicit secondary feature");

        ExplicitSecondaryFeatureExtractorManager explicitManager = new ExplicitSecondaryFeatureExtractorManager();

        List<DateFeature> dateFeatures = cepfRdd.map(CompositeExplicitPrimaryFeature::getDateFeature).collect();
        UsualEmailTimeFeature usualEmailTimeFeature = explicitManager.computeUsualEmailTimeFeature(dateFeatures);

        List<EmailLengthFeature> emailLengthFeatures = cepfRdd.map(CompositeExplicitPrimaryFeature::getEmailLengthFeature).collect();
        AvgEmailLengthFeature avgEmailLengthFeature = explicitManager.computeAvgEmailLengthFeature(emailLengthFeatures);

        List<ArtifactRelevanceFeature> orclRelevanceFeatures = new ArrayList<>(cepfRdd.map(CompositeExplicitPrimaryFeature::getOrclRelevance).collect());
        TechCompanyRelevanceFeature techCompanyRelevanceFeature = explicitManager.computeTechCompanyRelevanceFeature(orclRelevanceFeatures);

        List<ArtifactRelevanceFeature> sberRelevanceFeatures = new ArrayList<>(cepfRdd.map(CompositeExplicitPrimaryFeature::getSberRelevance).collect());
        FinanceCompanyRelevanceFeature financeCompanyRelevanceFeature = explicitManager.computeFinanceCompanyRelevanceFeature(sberRelevanceFeatures);

        CompositeExplicitSecondaryFeature compositeExplicitSecondaryFeature =
            new CompositeExplicitSecondaryFeature(usualEmailTimeFeature, avgEmailLengthFeature,
                    techCompanyRelevanceFeature, financeCompanyRelevanceFeature);

        LOG.debug("Explicit secondary feature is calculated: {}", compositeExplicitSecondaryFeature.toString());

        LOG.debug("Persisting secondary feature to {}", mySqlDataSource.getConnectionString());
        Dataset<Row> dataFrame = sqlContext.createDataFrame(Collections
            .singletonList(compositeExplicitSecondaryFeature.toPersistableType()),
                CompositeExplicitSecondaryFeature.STRUCTURED_TYPE);

        dataFrame.write()
            .mode(SaveMode.Append)
            .jdbc(mySqlDataSource.getConnectionString(), CompositeExplicitSecondaryFeature.TABLE_NAME,
                mySqlDataSource.getConnectionProperties());

        LOG.debug("Persisted");
    }
}
