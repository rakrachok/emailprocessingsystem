package org.behavoxtest.processing.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.ArtifactRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.CompositePrimaryFeature;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Jackson based json parser. It is used to serialize/deserialize composite primary features before putting them into HDFS, etc
 * Object mapper is initialized in the methods to avoid "Task not serializable" or concurrency issues
 */
public class CompositePrimaryFeatureJsonParser implements Serializable {

    public String toJson(CompositePrimaryFeature compositePrimaryFeature) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        Map<Class<? extends PrimaryFeature>, PrimaryFeature> classPrimaryFeatureMap = compositePrimaryFeature.toPersistableType();
        Map<Class<? extends PrimaryFeature>, String> preparedToSerialize = new HashMap<>();
        for (Map.Entry<Class<? extends PrimaryFeature>, PrimaryFeature> entry : classPrimaryFeatureMap.entrySet()) {
            preparedToSerialize.put(entry.getKey(), mapper.writeValueAsString(entry.getValue()));
        }
        return mapper.writeValueAsString(preparedToSerialize);
    }

    public CompositePrimaryFeature toObject(String json) throws IOException, ClassNotFoundException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        Map<String, String> hashMap = mapper.readValue(json, HashMap.class);

        String id = null;
        List<PrimaryFeature> primaryFeatures = new ArrayList<>();
        List<ArtifactRelevanceFeature> artifactRelevanceFeatures = new ArrayList<>();
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            Class<?> clazz = Class.forName(entry.getKey());
            Object value = mapper.readValue(entry.getValue(), clazz);
            if (String.class.equals(clazz)) {
                id = (String) value;
            } else if(ArtifactRelevanceFeature.class.equals(clazz.getSuperclass())) {
                artifactRelevanceFeatures.add((ArtifactRelevanceFeature) value);
            } else if(PrimaryFeature.class.equals(clazz.getSuperclass())) {
                primaryFeatures.add((PrimaryFeature) value);
            }
        }

        return new CompositePrimaryFeature(id, primaryFeatures, artifactRelevanceFeatures);
    }
}
