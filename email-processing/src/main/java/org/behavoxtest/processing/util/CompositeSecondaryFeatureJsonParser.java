package org.behavoxtest.processing.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.behavoxtest.core.processing.analytics.feature.SecondaryFeature;
import org.behavoxtest.processing.analytics.secondary.feature.CompositeSecondaryFeature;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Jackson based json parser. It is used to serialize composite secnondary features before putting them into HDFS, etc
 * Object mapper is initialized in the methods to avoid "Task not serializable" or concurrency issues
 */
public class CompositeSecondaryFeatureJsonParser implements Serializable {

    public String toJson(CompositeSecondaryFeature compositeSecondaryFeature) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        Map<Class<? extends SecondaryFeature>, SecondaryFeature> classSecondaryFeatureMap = compositeSecondaryFeature.toPersistableType();
        Map<Class<? extends SecondaryFeature>, String> preparedToSerialize = new HashMap<>();
        for (Map.Entry<Class<? extends SecondaryFeature>, SecondaryFeature> entry : classSecondaryFeatureMap.entrySet()) {
            preparedToSerialize.put(entry.getKey(), mapper.writeValueAsString(entry.getValue()));
        }
        return mapper.writeValueAsString(preparedToSerialize);
    }
}
