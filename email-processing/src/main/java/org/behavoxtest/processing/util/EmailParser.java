package org.behavoxtest.processing.util;

import org.apache.commons.mail.util.MimeMessageParser;
import org.behavoxtest.processing.analytics.common.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.stream.Collectors;

/**
 * A wrapper for Apache lib which parses EML files
 */
public class EmailParser {

    private static final Logger LOG = LoggerFactory.getLogger(EmailParser.class);

    public static Email parse(String filename, String email) {
        LOG.debug("Parsing email");

        try(InputStream emailInputStream = new ByteArrayInputStream(email.getBytes())) {
            MimeMessage emailMessage = new MimeMessage(null, emailInputStream);
            MimeMessageParser parser = new MimeMessageParser(emailMessage).parse();
            Email result = createEmail(filename, parser);
            result.setBodyLength(email.length());
            LOG.debug("Email has been successfully parsed");
            return result;
        } catch (Exception e) {
            LOG.warn("Email parsing failed", e);
            throw new RuntimeException(e);
        }
    }

    private static Email createEmail(String filename, MimeMessageParser parser) throws Exception {
        Email email = new Email();
        email.setEmailId(filename);

        Instant instant = parser.getMimeMessage().getSentDate().toInstant();
        LocalDateTime date = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        String sentDate = date.toString();
        email.setSentDate(sentDate);

        email.setFrom(parser.getFrom());
        email.setTo(parser.getTo().stream()
            .map(Address::toString).collect(Collectors.toList()));
        email.setCc(parser.getCc().stream().map(Address::toString).collect(Collectors.toList()));
        email.setSubject(parser.getSubject());
        email.setContent(parser.getPlainContent());
        email.setAttachments(parser.getAttachmentList().stream()
            .collect(Collectors.toMap(DataSource::getName, DataSource::getContentType)));

        return email;
    }
}