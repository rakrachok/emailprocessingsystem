package org.behavoxtest.processing;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({EmailProcessingConfiguration.class})
@ComponentScan("org.behavoxtest.processing.*")
public class TestProcessingConfiguration {
}
