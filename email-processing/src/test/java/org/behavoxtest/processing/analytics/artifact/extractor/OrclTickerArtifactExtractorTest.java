package org.behavoxtest.processing.analytics.artifact.extractor;

import org.behavoxtest.processing.analytics.artifact.OrclTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;


public class OrclTickerArtifactExtractorTest extends TickerArtifactExtractorTest {
    private final String TICKER = "ORCL";

    @Override
    protected String getTicker() {
        return TICKER;
    }

    @Override
    protected TickerArtifact getExpectedTickerArtifact(Email email, int pos) {
        return new OrclTickerArtifact(email, TICKER, pos);
    }

    @Override
    protected TickerArtifactExtractor getTickerArtifactExtractor() {
        return new OrclTickerArtifactExtractor();
    }
}
