package org.behavoxtest.processing.analytics.artifact.extractor;

import org.behavoxtest.processing.analytics.artifact.SberTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;

public class SberTickerArtifactExtractorTest extends TickerArtifactExtractorTest {
    private static final String TICKER = "SBER";

    @Override
    protected String getTicker() {
        return TICKER;
    }

    @Override
    protected TickerArtifact getExpectedTickerArtifact(Email email, int pos) {
        return new SberTickerArtifact(email, TICKER, pos);
    }

    @Override
    protected TickerArtifactExtractor getTickerArtifactExtractor() {
        return new SberTickerArtifactExtractor();
    }
}
