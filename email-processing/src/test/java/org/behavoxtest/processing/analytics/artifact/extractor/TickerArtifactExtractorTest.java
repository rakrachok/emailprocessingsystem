package org.behavoxtest.processing.analytics.artifact.extractor;

import org.apache.commons.lang3.RandomStringUtils;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class TickerArtifactExtractorTest {

    private Email email;

    @Before
    public void setUp() {
        email = new Email();
        email.setEmailId(RandomStringUtils.randomAlphabetic(5));
        email.setContent(RandomStringUtils.randomAlphabetic(20) + " " + getTicker());
    }

    @Test
    public void applyTest() {
        TickerArtifact expectedArtifact = getExpectedTickerArtifact(email, 21);
        TickerArtifactExtractor tickerArtifactExtractor = getTickerArtifactExtractor();
        TickerArtifact actualArtifact = tickerArtifactExtractor.apply(email);
        Assert.assertNotNull(actualArtifact);
        Assert.assertEquals(expectedArtifact.getTicker(), actualArtifact.getTicker());
        Assert.assertEquals(expectedArtifact.getPos(), actualArtifact.getPos());
    }

    protected abstract String getTicker();

    protected abstract TickerArtifact getExpectedTickerArtifact(Email email, int pos);

    protected abstract TickerArtifactExtractor getTickerArtifactExtractor();
}
