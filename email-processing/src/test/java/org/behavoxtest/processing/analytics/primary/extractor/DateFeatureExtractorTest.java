package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;

import java.time.LocalDateTime;
import java.util.Optional;

public class DateFeatureExtractorTest  extends PrimaryFeatureExtractorTest {

    @Override
    protected TickerArtifact getExpectedTickerArtifact() {
        return null;
    }

    @Override
    protected PrimaryFeature getExpectedPrimaryFeature() {
        return new DateFeature(LocalDateTime.parse(email.getSentDate()));
    }

    @Override
    protected Optional<GeneralInfoFeatureExtractor> getGeneralInfoFeatureExtractor() {
        return Optional.of(new DateFeatureExtractor());
    }

    @Override
    protected Optional<ArtifactRelevanceFeatureExtractor> getArtifactRelevanceFeatureExtractor() {
        return Optional.empty();
    }
}
