package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;

import java.util.Optional;

public class EmailLengthFeatureExtractorTest extends PrimaryFeatureExtractorTest {

    @Override
    protected Optional<GeneralInfoFeatureExtractor> getGeneralInfoFeatureExtractor() {
        return Optional.of(new EmailLengthFeatureExtractor());
    }

    @Override
    protected Optional<ArtifactRelevanceFeatureExtractor> getArtifactRelevanceFeatureExtractor() {
        return Optional.empty();
    }

    @Override
    protected TickerArtifact getExpectedTickerArtifact() {
        return null;
    }

    @Override
    protected PrimaryFeature getExpectedPrimaryFeature() {
        EmailLengthFeature elf = new EmailLengthFeature(email.getBodyLength());
        return elf;
    }
}
