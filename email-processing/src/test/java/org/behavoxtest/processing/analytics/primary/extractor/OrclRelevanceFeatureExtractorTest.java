package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.OrclTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;

import java.util.Optional;

public class OrclRelevanceFeatureExtractorTest  extends PrimaryFeatureExtractorTest {

    private final static String ORCL = "ORCL";
    private int pos;

    @Override
    protected Optional<GeneralInfoFeatureExtractor> getGeneralInfoFeatureExtractor() {
        return Optional.empty();
    }

    @Override
    protected Optional<ArtifactRelevanceFeatureExtractor> getArtifactRelevanceFeatureExtractor() {
        return Optional.of(new OrclRelevanceFeatureExtractor());
    }

    @Override
    protected TickerArtifact getExpectedTickerArtifact() {
        pos = email.getContent().length();
        email.setContent(email.getContent() + ORCL);
        return new OrclTickerArtifact(email, ORCL, pos);
    }

    @Override
    protected PrimaryFeature getExpectedPrimaryFeature() {
        double relevance = (double) pos / email.getContent().length();
        return new OrclRelevanceFeature(relevance);
    }
}
