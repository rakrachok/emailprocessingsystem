package org.behavoxtest.processing.analytics.primary.extractor;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.common.Email;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

public abstract class PrimaryFeatureExtractorTest {

    protected Email email;

    private TickerArtifact tickerArtifact;

    @Before
    public void setUp() {
        email = getExpectedEmail();
        tickerArtifact = getExpectedTickerArtifact();
    }

    @Test
    public void applyTest() {
        Optional<GeneralInfoFeatureExtractor> generalInfoFeatureExtractor = getGeneralInfoFeatureExtractor();
        generalInfoFeatureExtractor.ifPresent(extractor -> {
            PrimaryFeature expectedPrimaryFeature = getExpectedPrimaryFeature();
            PrimaryFeature actualPrimaryFeature = extractor.apply(email);
            Assert.assertNotNull(actualPrimaryFeature);
            Assert.assertEquals(expectedPrimaryFeature.getValue(), actualPrimaryFeature.getValue());
        });

        Optional<ArtifactRelevanceFeatureExtractor> artifactRelevanceFeatureExtractor = getArtifactRelevanceFeatureExtractor();
        artifactRelevanceFeatureExtractor.ifPresent(extractor -> {
            PrimaryFeature expectedPrimaryFeature = getExpectedPrimaryFeature();
            PrimaryFeature actualPrimaryFeature = extractor.apply(tickerArtifact);
            Assert.assertNotNull(actualPrimaryFeature);
            Assert.assertEquals(expectedPrimaryFeature.getValue(), actualPrimaryFeature.getValue());
        });
    }

    protected Email getExpectedEmail() {
        Email email = new Email();
        email.setEmailId(RandomStringUtils.randomAlphabetic(5));
        email.setContent(RandomStringUtils.randomAlphabetic(20));
        email.setBodyLength(RandomUtils.nextInt());

        Instant now = Instant.now();
        LocalDateTime date = now.atZone(ZoneId.systemDefault()).toLocalDateTime();
        String sentDate = date.toString();
        email.setSentDate(sentDate);
        return email;
    }

    protected abstract TickerArtifact getExpectedTickerArtifact();

    protected abstract PrimaryFeature getExpectedPrimaryFeature();

    protected abstract Optional<GeneralInfoFeatureExtractor> getGeneralInfoFeatureExtractor();

    protected abstract Optional<ArtifactRelevanceFeatureExtractor> getArtifactRelevanceFeatureExtractor();
}
