package org.behavoxtest.processing.analytics.primary.extractor;

import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.artifact.SberTickerArtifact;
import org.behavoxtest.processing.analytics.artifact.TickerArtifact;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;

import java.util.Optional;

public class SberRelevanceFeatureExtractorTest  extends PrimaryFeatureExtractorTest {

    private final static String SBER = "SBER";
    private int pos;

    @Override
    protected Optional<GeneralInfoFeatureExtractor> getGeneralInfoFeatureExtractor() {
        return Optional.empty();
    }

    @Override
    protected Optional<ArtifactRelevanceFeatureExtractor> getArtifactRelevanceFeatureExtractor() {
        return Optional.of(new SberRelevanceFeatureExtractor());
    }

    @Override
    protected TickerArtifact getExpectedTickerArtifact() {
        pos = email.getContent().length();
        email.setContent(email.getContent() + SBER);
        return new SberTickerArtifact(email, SBER, pos);
    }

    @Override
    protected PrimaryFeature getExpectedPrimaryFeature() {
        double relevance = (double) pos / email.getContent().length();
        return new OrclRelevanceFeature(relevance);
    }
}
