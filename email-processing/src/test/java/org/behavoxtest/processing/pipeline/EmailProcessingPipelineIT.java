package org.behavoxtest.processing.pipeline;

import org.behavoxtest.core.processing.pipeline.Pipeline;
import org.behavoxtest.processing.AbstractIT;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class EmailProcessingPipelineIT extends AbstractIT {
//TODO: implement properly
    @Autowired
    @Qualifier("emailProcessingPipeline")
    private Pipeline emailProcessingPipeline;

    @Test
    public void getTest() {
        emailProcessingPipeline.start();
    }


}
