package org.behavoxtest.processing.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomUtils;
import org.behavoxtest.core.processing.analytics.feature.PrimaryFeature;
import org.behavoxtest.processing.analytics.primary.feature.DateFeature;
import org.behavoxtest.processing.analytics.primary.feature.EmailLengthFeature;
import org.behavoxtest.processing.analytics.primary.feature.OrclRelevanceFeature;
import org.behavoxtest.processing.analytics.primary.feature.SberRelevanceFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

@RunWith(Parameterized.class)
public class CompositePrimaryFeatureJsonParserTest {

    @Parameterized.Parameters
    public static Iterable<Object> features() {
        return Arrays.asList(new DateFeature(LocalDateTime.now()), new EmailLengthFeature(RandomUtils.nextInt()), new OrclRelevanceFeature(RandomUtils.nextDouble()),
            new SberRelevanceFeature(RandomUtils.nextDouble()));
    }

    @Parameterized.Parameter
    public PrimaryFeature expectedPrimaryFeature;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
    }

    @Test
    public void testPrimaryFeature() throws IOException {
        String json = mapper.writeValueAsString(expectedPrimaryFeature);
        PrimaryFeature actualPrimaryFeature = mapper.readValue(json, expectedPrimaryFeature.getClass());
        Assert.assertNotNull(actualPrimaryFeature);
        Assert.assertEquals(expectedPrimaryFeature.getValue(), actualPrimaryFeature.getValue());
    }
}
