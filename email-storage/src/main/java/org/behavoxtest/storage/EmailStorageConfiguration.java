package org.behavoxtest.storage;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.behavoxtest.core.filestorage.spring.FileStorageConfiguration;

/**
 * Email storage specific config
 */
@Configuration
@Import({HdfsConfiguration.class, MySqlConfiguration.class})
public class EmailStorageConfiguration implements FileStorageConfiguration {
}
