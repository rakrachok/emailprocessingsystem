package org.behavoxtest.storage;

import org.behavoxtest.core.filestorage.dao.Dao;
import org.behavoxtest.storage.dao.hdfs.HdfsDaoImpl;
import org.behavoxtest.storage.datasource.hdfs.HdfsDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * HDFS config
 */
@Configuration
@PropertySource("classpath:storage.properties")
public class HdfsConfiguration {

    @Value("${emailstorage.hdfs.connection.url}")
    private String connectionUrl;

    @Value("${emailstorage.hdfs.incoming.path}")
    private String incomingPath;

    @Value("${emailstorage.hdfs.primaryfeature.path}")
    private String primaryFeaturePath;

    @Value("${emailstorage.hdfs.secondaryfeature.path}")
    private String secondaryFeaturePath;

    @Bean
    public org.apache.hadoop.conf.Configuration conf() {
        return new org.apache.hadoop.conf.Configuration(true);
    }

    @Bean
    public HdfsDataSource incomingHdfsDataSource() throws IOException, URISyntaxException {
        return new HdfsDataSource(connectionUrl, incomingPath, conf());
    }

    @Bean
    public HdfsDataSource primaryFeatureHdfsDataSource() throws IOException, URISyntaxException {
        return new HdfsDataSource(connectionUrl, primaryFeaturePath, conf());
    }

    @Bean
    public HdfsDataSource secondaryFeatureHdfsDataSource() throws IOException, URISyntaxException {
        return new HdfsDataSource(connectionUrl, secondaryFeaturePath, conf());
    }

    @Bean
    public Dao<String, URI> fileStorageDao() throws IOException, URISyntaxException {
        return new HdfsDaoImpl(incomingHdfsDataSource());
    }
}
