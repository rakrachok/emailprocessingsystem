package org.behavoxtest.storage;

import org.behavoxtest.core.filestorage.datasource.DataSource;
import org.behavoxtest.storage.datasource.mysql.MySqlDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Properties;

/**
 * MySql config
 */
@Configuration
@PropertySource("classpath:storage.properties")
public class MySqlConfiguration {

    @Value("${emailstorage.mysql.connection.url}")
    private String mysqlUrl;

    @Value("${emailstorage.mysql.connection.user}")
    private String user;

    @Value("${emailstorage.mysql.connection.password}")
    private String password;

    @Bean
    public DataSource mysqlDataSource() {
        Properties properties = new Properties();
        properties.setProperty("user", user);
        properties.setProperty("password", password);
        return new MySqlDataSource(mysqlUrl, properties);
    }
}
