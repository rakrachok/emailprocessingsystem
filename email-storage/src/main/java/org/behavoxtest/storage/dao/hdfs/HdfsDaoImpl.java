package org.behavoxtest.storage.dao.hdfs;

import org.apache.hadoop.fs.Path;
import org.behavoxtest.core.filestorage.dao.Dao;
import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.behavoxtest.storage.datasource.hdfs.HdfsDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

/**
 * Implementation of Dao.class  to perofrm isolated non-streaming I/O with HDFS
 */
public class HdfsDaoImpl implements Dao<String, URI> {

    private final static Logger LOG = LoggerFactory.getLogger(HdfsDaoImpl.class);

    private HdfsDataSource dataSource;

    public HdfsDaoImpl(HdfsDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void readTo(FileStorageEntityHolder<String, URI> fileStorageEntityHolder) {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public void persist(FileStorageEntityHolder<String, URI> fileStorageEntityHolder) throws DataAccessException {
        LOG.debug("Persisting resource with URI: {}", fileStorageEntityHolder.getEntity());
        Path filePath = new Path(fileStorageEntityHolder.getEntity());
        Path destinationPath = dataSource.createDestinationPath(fileStorageEntityHolder.getId());
        try {
            dataSource.getDfs().copyFromLocalFile(filePath, destinationPath);
        } catch (IOException e) {
            LOG.error("Error occurred when storing the resource", e);
            throw new DataAccessException("Error occurred when storing the resource caused by: " + e.getMessage(), e);
        }
        LOG.debug("The resource has been successfully persisted: {}", destinationPath.toString());
    }

    @Override
    public void delete(FileStorageEntityHolder<String, URI> fileStorageEntityHolder) throws DataAccessException {
        LOG.debug("Deleting resource with ID: {}", fileStorageEntityHolder.getId());
        Path filePath = dataSource.createDestinationPath(fileStorageEntityHolder.getId());
        try {
            dataSource.getDfs().delete(filePath, true);
        } catch (IOException e) {
            LOG.error("Error occurred when deleting the resource", e);
            throw new DataAccessException("Error occurred when deleting the resource caused by: " + e.getMessage(), e);
        }
    }
}
