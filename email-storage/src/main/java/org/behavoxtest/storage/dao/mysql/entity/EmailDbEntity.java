package org.behavoxtest.storage.dao.mysql.entity;

import java.io.Serializable;

public class EmailDbEntity implements Serializable {

    private String emailId;

    private String sentDate;

    private String fromEmail;

    private String toEmail;

    private String ccEmail;

    private String subject;

    private String content;

    public EmailDbEntity(String emailId, String sentDate, String from, String to, String cc, String subject, String content) {
        this.emailId = emailId;
        this.sentDate = sentDate;
        this.fromEmail = from;
        this.toEmail = to;
        this.ccEmail = cc;
        this.subject = subject;
        this.content = content;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getCcEmail() {
        return ccEmail;
    }

    public void setCcEmail(String ccEmail) {
        this.ccEmail = ccEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}