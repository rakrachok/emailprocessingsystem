package org.behavoxtest.storage.dao.mysql.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class PrimaryFeatureDbEntity implements Serializable {
    private String emailId;
    private String dateFeature;
    private Integer emailLengthFeature;
    private Double orclRelevance;
    private Double sberRelevance;

    public PrimaryFeatureDbEntity(String emailId, LocalDateTime dateFeature, Integer emailLengthFeature, Double orclRelevance, Double sberRelevance) {
        this.emailId = emailId;
        this.dateFeature = dateFeature.toString();
        this.emailLengthFeature = emailLengthFeature;
        this.orclRelevance = orclRelevance;
        this.sberRelevance = sberRelevance;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getDateFeature() {
        return dateFeature;
    }

    public void setDateFeature(LocalDateTime dateFeature) {
        this.dateFeature = dateFeature.toString();
    }

    public Integer getEmailLengthFeature() {
        return emailLengthFeature;
    }

    public void setEmailLengthFeature(Integer emailLengthFeature) {
        this.emailLengthFeature = emailLengthFeature;
    }

    public Double getOrclRelevance() {
        return orclRelevance;
    }

    public void setOrclRelevance(Double orclRelevance) {
        this.orclRelevance = orclRelevance;
    }

    public Double getSberRelevance() {
        return sberRelevance;
    }

    public void setSberRelevance(Double sberRelevance) {
        this.sberRelevance = sberRelevance;
    }
}