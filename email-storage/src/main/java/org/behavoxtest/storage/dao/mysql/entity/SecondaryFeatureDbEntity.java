package org.behavoxtest.storage.dao.mysql.entity;

import java.io.Serializable;

public class SecondaryFeatureDbEntity implements Serializable {
    private String usualEmailTime;
    private Double avgEmailLength;
    private Double techCompanyRelevance;
    private Double financeCompanyRelevance;

    public SecondaryFeatureDbEntity(String usualEmailTime, Double avgEmailLength, Double techCompanyRelevance, Double financeCompanyRelevance) {
        this.usualEmailTime = usualEmailTime;
        this.avgEmailLength = avgEmailLength;
        this.techCompanyRelevance = techCompanyRelevance;
        this.financeCompanyRelevance = financeCompanyRelevance;
    }

    public String getUsualEmailTime() {
        return usualEmailTime;
    }

    public void setUsualEmailTime(String usualEmailTime) {
        this.usualEmailTime = usualEmailTime;
    }

    public Double getAvgEmailLength() {
        return avgEmailLength;
    }

    public void setAvgEmailLength(Double avgEmailLength) {
        this.avgEmailLength = avgEmailLength;
    }

    public Double getTechCompanyRelevance() {
        return techCompanyRelevance;
    }

    public void setTechCompanyRelevance(Double techCompanyRelevance) {
        this.techCompanyRelevance = techCompanyRelevance;
    }

    public Double getFinanceCompanyRelevance() {
        return financeCompanyRelevance;
    }

    public void setFinanceCompanyRelevance(Double financeCompanyRelevance) {
        this.financeCompanyRelevance = financeCompanyRelevance;
    }
}