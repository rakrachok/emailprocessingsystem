package org.behavoxtest.storage.dao.mysql.entity;

import java.io.Serializable;

public class TickerArtifactDbEntity implements Serializable {

    private String emailId;
    private String ticker;
    private Integer pos;

    public TickerArtifactDbEntity(String emailId, String ticker, Integer pos) {
        this.emailId = emailId;
        this.ticker = ticker;
        this.pos = pos;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }
}
