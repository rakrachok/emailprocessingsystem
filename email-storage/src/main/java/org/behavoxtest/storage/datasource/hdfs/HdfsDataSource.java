package org.behavoxtest.storage.datasource.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.behavoxtest.core.filestorage.datasource.DataSource;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * DataSource for HDFS related operations
 */
public class HdfsDataSource implements DataSource {

    private final String hdfsUri;
    private final FileSystem dfs;
    private final Path incomePath;
    private final Configuration conf;

    public HdfsDataSource(String hdfsUri, String incomeDir, Configuration conf) throws URISyntaxException, IOException {
        this.conf = conf;
        this.hdfsUri = hdfsUri;
        this.dfs = FileSystem.get(new URI(hdfsUri), this.conf);
        this.incomePath = new Path(hdfsUri + incomeDir);
    }

    public String getHdfsUri() {
        return hdfsUri;
    }

    public FileSystem getDfs() {
        return dfs;
    }

    public Path getIncomePath() {
        return incomePath;
    }

    public Path createDestinationPath(String id) {
        return new Path(incomePath, id);
    }

    @Override
    public String getConnectionString() {
        return incomePath.toUri().toString();
    }
}
