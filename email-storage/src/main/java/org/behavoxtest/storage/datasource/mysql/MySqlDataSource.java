package org.behavoxtest.storage.datasource.mysql;

import org.behavoxtest.core.filestorage.datasource.DataSource;

import java.util.Properties;

/**
 * DataSource for MySql related operations
 */
public class MySqlDataSource implements DataSource {

    private String mysqlUrl;

    private final Properties properties;

    public MySqlDataSource(String mysqlUrl, Properties properties) {
        this.mysqlUrl = mysqlUrl;
        this.properties = properties;
    }

    @Override
    public String getConnectionString() {
        return mysqlUrl;
    }

    @Override
    public Properties getConnectionProperties() {
        return properties;
    }
}
