package org.behavoxtest.storage.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.behavoxtest.core.filestorage.dao.Dao;
import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.behavoxtest.core.filestorage.service.FileStorageService;

import java.net.URI;

/**
 * Service provider functionality for HDFS I/O
 * Used by app module to perform single persistence operation
 */
@Service
public class EmailStorageService implements FileStorageService {

    private final static Logger LOG = LoggerFactory.getLogger(EmailStorageService.class);

    @Autowired
    @Qualifier("fileStorageDao")
    private Dao<String, URI> fileStorageDao;

    public void persist(String locator, URI resource) throws DataAccessException {
        FileStorageEntityHolder<String, URI> fileStorageEntityHolder = new FileStorageEntityHolder<>(locator, resource);
        fileStorageDao.persist(fileStorageEntityHolder);
    }

    public void delete(String locator, URI resource) throws DataAccessException {
        FileStorageEntityHolder<String, URI> fileStorageEntityHolder = new FileStorageEntityHolder<>(locator, resource);
        fileStorageDao.delete(fileStorageEntityHolder);
    }
}
