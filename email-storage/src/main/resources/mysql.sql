//user root
create database behavox;
grant all on behavox.* to 'behavox'@'localhost'



//user behavox
drop table if exists behavox.email;
create table behavox.email (
    emailId varchar(200),
    sentDate varchar(30),
    fromEmail varchar(100),
    toEmail text,
    ccEmail text,
    subject text,
    content text,
    PRIMARY KEY (emailId)
);
drop table if exists behavox.artifact;
create table behavox.artifact (
    emailId varchar(200),
    ticker varchar(30),
    pos int,
    PRIMARY KEY (emailId, ticker, pos)
);
drop table if exists behavox.primary_feature;
create table behavox.primary_feature (
    emailId varchar(200),
    dateFeature varchar(30),
    emailLengthFeature int,
    orclRelevance double,
    sberRelevance double
);
drop table if exists behavox.secondary_feature;
create table behavox.secondary_feature (
    usualEmailTime varchar(50),
    avgEmailLength double,
    techCompanyRelevance double,
    financeCompanyRelevance double
);