package org.behavoxtest.storage;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({EmailStorageConfiguration.class})
@ComponentScan("org.behavoxtest.storage.*")
public class TestStorageConfiguration {
}
