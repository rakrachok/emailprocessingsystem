package org.behavoxtest.storage.dao;


import org.apache.commons.lang3.RandomStringUtils;
import org.apache.hadoop.fs.Path;
import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.behavoxtest.storage.AbstractIT;
import org.behavoxtest.storage.dao.hdfs.HdfsDaoImpl;
import org.behavoxtest.storage.datasource.hdfs.HdfsDataSource;
import org.behavoxtest.storage.service.EmailStorageServiceIT;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HdfsDaoImplIT extends AbstractIT {

    @Autowired
    HdfsDataSource incomingHdfsDataSource;

    @Autowired
    private HdfsDaoImpl hdfsDao;

    String fileLocator;

    URI emailUri;

    private FileStorageEntityHolder<String, URI> entityHolder;

    @Before
    public void setUp() throws URISyntaxException {
        fileLocator = EmailStorageServiceIT.class.getSimpleName() + ".eml";
        emailUri = EmailStorageServiceIT.class.getResource(fileLocator).toURI();
        entityHolder = new FileStorageEntityHolder<>(getClass().getSimpleName() + ".eml", emailUri);
    }

    @Test
    public void persistTestOk() throws DataAccessException, IOException {
        hdfsDao.persist(entityHolder);
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(entityHolder.getId());
        boolean exists = incomingHdfsDataSource.getDfs().exists(destinationPath);
        Assert.assertTrue(exists);
    }

    @Test(expected = DataAccessException.class)
    public void persistTestNOk() throws DataAccessException {
        URI uri = URI.create(RandomStringUtils.randomAlphabetic(5));
        FileStorageEntityHolder<String, URI> entity = new FileStorageEntityHolder<>(entityHolder.getId(), uri);
        hdfsDao.persist(entity);
    }

    @Test
    public void deleteTestOk() throws DataAccessException, IOException {
        hdfsDao.persist(entityHolder);
        hdfsDao.delete(entityHolder);
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(entityHolder.getId());
        boolean exists = incomingHdfsDataSource.getDfs().exists(destinationPath);
        Assert.assertFalse(exists);
    }

    @After
    public void after() throws DataAccessException, IOException {
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(entityHolder.getId());
        boolean exists = incomingHdfsDataSource.getDfs().exists(destinationPath);
        if (exists) {
            hdfsDao.delete(entityHolder);
        }
    }
}
