package org.behavoxtest.storage.dao;


import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.behavoxtest.storage.dao.hdfs.HdfsDaoImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

@RunWith(MockitoJUnitRunner.class)
public class HdfsDaoImplTest {

    @Mock
    private HdfsDaoImpl hdfsDao;

    String fileLocator;

    URI emailUri;

    private FileStorageEntityHolder<String, URI> entityHolder;

    @Before
    public void setUp() {
        fileLocator = getClass().getSimpleName() + ".eml";
        emailUri = URI.create(fileLocator);
        entityHolder = new FileStorageEntityHolder<>(getClass().getSimpleName() + ".eml", emailUri);
    }

    @Test
    public void persistTestOk() throws DataAccessException {
        hdfsDao.persist(entityHolder);
        Assert.assertNotNull(entityHolder);
        Assert.assertEquals(fileLocator, entityHolder.getId());
        Assert.assertEquals(emailUri, entityHolder.getEntity());
    }

    @Test(expected = DataAccessException.class)
    public void persistTestNOk() throws DataAccessException {
        Mockito.doThrow(new DataAccessException("")).when(hdfsDao).persist(entityHolder);
        hdfsDao.persist(entityHolder);
    }

    @Test
    public void deleteTestOk() throws DataAccessException {
        hdfsDao.delete(entityHolder);
        Assert.assertNotNull(entityHolder);
        Assert.assertEquals(fileLocator, entityHolder.getId());
        Assert.assertEquals(emailUri, entityHolder.getEntity());
    }

    @Test(expected = DataAccessException.class)
    public void deleteTestNOk() throws DataAccessException {
        Mockito.doThrow(new DataAccessException("")).when(hdfsDao).delete(entityHolder);
        hdfsDao.delete(entityHolder);
    }
}
