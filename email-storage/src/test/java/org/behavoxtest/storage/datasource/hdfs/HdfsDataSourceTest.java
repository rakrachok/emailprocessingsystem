package org.behavoxtest.storage.datasource.hdfs;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class HdfsDataSourceTest {

    private HdfsDataSource incomingHdfsDataSource;

    String hdfsUri = "hdfs://localhost:9000";

    private String inDir = "/test";

    @Before
    public void setUp() throws IOException, URISyntaxException {
        incomingHdfsDataSource = new HdfsDataSource(hdfsUri, inDir, new HdfsConfiguration());
    }

    @Test
    public void test() {
        String fileLocator = RandomStringUtils.randomAlphabetic(5);
        String expectedPath = hdfsUri + inDir + "/" + fileLocator;
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(fileLocator);
        Assert.assertNotNull(destinationPath);
        Assert.assertEquals(expectedPath, destinationPath.toUri().toString());
    }
}
