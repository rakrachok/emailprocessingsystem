package org.behavoxtest.storage.service;

import org.apache.hadoop.fs.Path;
import org.behavoxtest.core.filestorage.service.FileStorageService;
import org.behavoxtest.storage.AbstractIT;
import org.behavoxtest.storage.datasource.hdfs.HdfsDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class EmailStorageServiceIT extends AbstractIT {

    @Autowired
    private FileStorageService storageService;

    @Autowired
    private HdfsDataSource incomingHdfsDataSource;

    private String fileLocator;

    private URI emailUri;

    @Before
    public void setUp() throws URISyntaxException {
        fileLocator = getClass().getSimpleName() + ".eml";
        emailUri = getClass().getResource(fileLocator).toURI();
    }

    @Test
    public void persistTest() throws DataAccessException, IOException {
        storageService.persist(fileLocator, emailUri);
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(fileLocator);
        Assert.assertTrue(incomingHdfsDataSource.getDfs().exists(destinationPath));
    }

    @Test
    public void deleteTest() throws DataAccessException, IOException {
        storageService.persist(fileLocator, emailUri);
        storageService.delete(fileLocator, emailUri);
        Path destinationPath = incomingHdfsDataSource.createDestinationPath(fileLocator);
        Assert.assertFalse(incomingHdfsDataSource.getDfs().exists(destinationPath));
    }

    @After
    public void afterClass() throws DataAccessException {
        storageService.delete(fileLocator, emailUri);
    }
}
