package org.behavoxtest.storage.service;

import org.behavoxtest.core.filestorage.dao.Dao;
import org.behavoxtest.core.filestorage.dao.entity.FileStorageEntityHolder;
import org.behavoxtest.core.filestorage.dao.exception.DataAccessException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EmailStorageServiceTest {

    @Mock
    private Dao<String, URI> fileStorageDao;

    @InjectMocks
    private EmailStorageService storageService;

    private String fileLocator;

    private URI emailUri;

    @Before
    public void setUp() {
        fileLocator = getClass().getSimpleName() + ".eml";
        emailUri  = URI.create(fileLocator);
    }

    @Test
    public void persistTestOk() throws DataAccessException {
        FileStorageEntityHolder<String, URI> entityHolder = new FileStorageEntityHolder<>(fileLocator, emailUri);
        Mockito.doNothing().when(fileStorageDao).persist(entityHolder);
        storageService.persist(fileLocator, emailUri);
        Assert.assertEquals(fileLocator, entityHolder.getId());
        Assert.assertEquals(emailUri, entityHolder.getEntity());
    }

    @Test
    public void deleteTestOk() throws DataAccessException {
        FileStorageEntityHolder<String, URI> entityHolder = new FileStorageEntityHolder<>(fileLocator, emailUri);
        Mockito.doNothing().when(fileStorageDao).persist(entityHolder);
        storageService.delete(fileLocator, emailUri);
        Assert.assertEquals(fileLocator, entityHolder.getId());
        Assert.assertEquals(emailUri, entityHolder.getEntity());
    }

    @Test(expected = DataAccessException.class)
    public void persistTestNOk() throws DataAccessException {
        FileStorageEntityHolder<String, URI> fileStorageEntityHolder = new FileStorageEntityHolder<>(fileLocator, emailUri);
        Mockito.doThrow(new DataAccessException("Test")).when(fileStorageDao).persist(fileStorageEntityHolder);
        storageService.persist(fileLocator, emailUri);
    }

    @Test(expected = DataAccessException.class)
    public void deleteTestNOk() throws DataAccessException {
        FileStorageEntityHolder<String, URI> fileStorageEntityHolder = new FileStorageEntityHolder<>(fileLocator, emailUri);
        Mockito.doThrow(new DataAccessException("Test")).when(fileStorageDao).delete(fileStorageEntityHolder);
        storageService.delete(fileLocator, emailUri);
    }
}
